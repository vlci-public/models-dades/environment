# environment

Models de dades del projecte VLCi basats en els data models Fiware: https://github.com/smart-data-models/dataModel.Environment
These data models describe the main entities involved with smart applications that deal with environmental issues.

### List of data models

The following entity types are available:
- [AirQualityObserved](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/README.md). An observation of air quality conditions at a certain place and time.

- [NoiseLevelObserved](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/README.md). An observation of those acoustic parameters that estimate noise pressure levels at a certain place and time. 