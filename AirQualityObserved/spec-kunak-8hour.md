**Índice**  

 [[_TOC_]]  
  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `location`: Referencia Geojson al elemento. Tipo Point.
- `address`: La dirección postal.  
- `project`: proyecto al que pertenece esta entidad
- `description`: Una descripción de este artículo

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601 UTC. La fecha/hora es el final de la medición. Las mediciones son horarias.
- `coAvg8H`: Concentración promedio de monóxido de carbono en el aire durante las últimas 8 horas.
- `o3Avg8H`: Concentración promedio de ozono en el aire durante las últimas 8 horas.

##### Atributos de validación de las medidas
Cada atributo de medición tiene un flag asociado que describe su validez. Posibles valores:

| Flag | Descripción                                          |
|------|------------------------------------------------------|
|  T   |  Dato en bruto                                       |
|  TI  |  Dato auto invalidado por el software de Kunak Cloud |
|  I   |  Dato invalidado manualmente por un operador         |
|  V   |  Dato validado manualmente por un operador           |
|  O   |  Dato corregido en base a una calibración manual     |

- `coAvg8HFlag`
- `o3Avg8HFlag`

##### Atributos para la monitorización
- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData.
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad         |
|----------------|--------------------|
| /MedioAmbiente |  ECS_aire_8h       |