**Índice**  

 [[_TOC_]]  
  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `location`: Referencia Geojson al elemento. Tipo Point.
- `address`: La dirección postal.  
- `project`: proyecto al que pertenece esta entidad
- `description`: Una descripción de este artículo   

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601 UTC. La fecha/hora es el final de la medición. Las mediciones son cincominutales.
- `co2Corrected`: Concentración de CO2 corregida. Unidad de medida: ppm.
- `coCorrected`: Concentración de CO corregida. Unidad de medida: ppb.
- `no2Corrected`: Concentración de NO2 corregida. Unidad de medida: ppb.
- `noCorrected`: Concentración de NO corregida. Unidad de medida: ppb.
- `noxCorrected`: Concentración de NOX corregida. Unidad de medida: ppb.
- `o3Corrected`: Concentración de O3 corregida. Unidad de medida: ppb.
- `pm1`: Partículas de 1 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 1 µm.Unidad de medida: µg/m3.
- `pm25`: Partículas de 2,5 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 2.5 µm. Unidad de medida: µg/m3.
- `pm4`: Partículas de 4 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 4 µm. Unidad de medida: µg/m3.
- `pm10`: Partículas de 10 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro está aproximadamente entre 2.5 y 10 µm. Unidad de medida: µg/m3.
- `tpc`: Cantidad total de partículas suspendidas en un volumen específico de aire. Unidad de medida: counts/cm3.
- `tsp`: Concentración total de partículas suspendidas en el aire. Unidad de medida: µg/m3.

##### Atributos de validación de las medidas
Cada atributo de medición tiene un flag asociado que describe su validez. Posibles valores:

| Flag | Descripción                                          |
|------|------------------------------------------------------|
|  T   |  Dato en bruto                                       |
|  TI  |  Dato auto invalidado por el software de Kunak Cloud |
|  I   |  Dato invalidado manualmente por un operador         |
|  V   |  Dato validado manualmente por un operador           |
|  O   |  Dato corregido en base a una calibración manual     |

- `co2CorrectedFlag`
- `coCorrectedFlag`
- `no2CorrectedFlag`
- `noCorrectedFlag`
- `noxCorrectedFlag`
- `o3CorrectedFlag`
- `pm1Flag`
- `pm25Flag`
- `pm4Flag`
- `pm10Flag`
- `tpcFlag`
- `tspFlag`

##### Atributos para la monitorización
- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData.
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad         |
|----------------|--------------------|
| /MedioAmbiente |  ECS_aire_5m       |