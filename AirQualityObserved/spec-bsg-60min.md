**Índice**  

 [[_TOC_]]  
  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire en un lugar y momento determinados. Esta entidad es específica de BSG y se alimenta de una ETL que calcula la media cada hora a partir de los datos 10minutales proporcionados por BSG.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

**IMPORTANTE : No todas las estaciones disponen de todos los contaminantes, ver tabla inferior para conocer los contaminantes disponibles por estación.**

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `refPointOfInterest`: Entity ID, de tipo PointOfInterest, que describe la estación
- `address`: Texto con la calle donde se ubica la estación
- `calidad_ambiental`: Indica la calidad del aire de forma global que mide la estación. Calculado a partir de las mediciones de cada partícula. En FIWARE se corresponde con el atributo airQualityLevel. Posibles valores: Muy bueno, bueno, etc (ver leyenda en Valenciaalminut)
- `calidad_ambiental_id`: Identificador (del 1 al 5) asignado a cada uno de los niveles de calidad ambiental. En FIWARE se corresponde con el atributo: airQualityIndex

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid
- `dateObservedGMT0`: La fecha y la hora de esta observación en formato ISO8601 UTC  
- `NO2ValueOrigin`: Dióxido de nitrógeno detectado, tal y como viene del sistema origen (sin formatear)
- `NO2Value`: Dióxido de nitrógeno detectado, formateado a un número entero
- `NO2ValueFlag`: Posibles valores: V, F. V significa que la medición es Válida. Cualquier otro valor indica que la medición no es válida
- `O3ValueOrigin`: Ozono detectado, tal y como viene del sistema origen (sin formatear)
- `O3Value`: Ozono detectado, formateado a un número entero
- `O3ValueFlag`: Posibles valores: V, F. V significa que la medición es Válida. Cualquier otro valor indica que la medición no es válida
- `PM10ValueFlag`: Posibles valores: V, F. V significa que la medición es Válida. Cualquier otro valor indica que la medición no es válida
- `PM10Value`: Partículas de 10 micrómetros o menos de diámetro, formateado a un número entero
- `PM10ValueFull`: Partículas de 10 micrómetros o menos de diámetro.  <!-- TO-DO Explicar la diferencia con los otros dos valores (Iván) -->
- `PM10ValueOrigin`: Partículas de 10 micrómetros o menos de diámetro, tal y como viene del sistema origen (sin formatear)
- `PM25Value`: Partículas de 2,5 micrómetros o menos de diámetro, formateado a un número entero
- `PM25ValueFlag`: Posibles valores: V, F. V significa que la medición es Válida. Cualquier otro valor indica que la medición no es válida
- `PM25ValueOrigin`: Partículas de 2,5 micrómetros o menos de diámetro, tal y como viene del sistema origen (sin formatear)
- `SO2Value`: Dióxido de azufre detectado, formateado a un número entero
- `SO2ValueFlag`: Posibles valores: V, F. V significa que la medición es Válida. Cualquier otro valor indica que la medición no es válida
- `SO2ValueOrigin`: Dióxido de azufre detectado, tal y como viene del sistema origen (sin formatear)

##### Atributos descriptivos de la medición
- `NO2Description`: Descripción de la medición
- `NO2Name`: Nombre del contaminante (Ej: NO2)
- `NO2Type`: Unidad de medida de la medición (µg/m3)
- `O3Description`: Descripción de la medición
- `O3Name`: Nombre del contaminante (Ej: NO2)
- `O3Type`: Unidad de medida de la medición (µg/m3)
- `PM10CorrectionFactor`: Factor de corrección a aplicar a la medición de PM10
- `PM10Description`: Descripción de la medición
- `PM10Name`: Nombre del contaminante (Ej: NO2)
- `PM10Type`: Unidad de medida de la medición (µg/m3)
- `PM25Description`: Descripción de la medición
- `PM25Name`: Nombre del contaminante (Ej: NO2)
- `PM25Type`: Unidad de medida de la medición (µg/m3)
- `SO2Description`: Descripción de la medición
- `SO2Name`: Nombre del contaminante (Ej: NO2)
- `SO2Type`: Unidad de medida de la medición (µg/m3)

##### Atributos para realizar cálculos / lógica de negocio
- `NO2Alerta`: Valor por defecto: 400. Este valor indica la medida límite de NO2 para que salte el aviso. Utilizado para dar un tercer nivel de alerta por email.
- `NO2Aviso`: Valor por defecto: 200. Este valor indica la medida límite de NO2 para que salte el aviso. Utilizado para dar un segundo nbivel de alerta por email.
- `NO2Preaviso`: Valor por defecto: 180. Este valor indica la medida límite de NO2 para que salte el aviso. Utilizado para dar un primer nivel de alerta por email.
- `NO2DescriptionPrev`: Se guarda el valor de la medición anterior. Utilizado en las alertas.
- `NO2NamePrev`: Se guarda el valor de la medición anterior. Utilizado en las alertas.
- `NO2TypePrev`: Se guarda el valor de la medición anterior. Utilizado en las alertas.
- `NO2ValuePrev`: Se guarda el valor de la medición anterior. Utilizado en las alertas.
- `gis_id`: Nombre identificador en el esquema gis del postgres municipal. Posibles valores: Francia, Boulevar Sur, Molí del Sol, Pista de Silla, Universidad Politécnica, Viveros, Centro.
- `calculatedTimestamp`: Define el momento en el que la entidad ha sido actualizada con todos los datos calculados.

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             | Contaminantes Disponibles |
|----------------|------------------------|---------------------------|
| /MedioAmbiente | A01_AVFRANCIA_60m      |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A02_BULEVARDSUD_60m    |NO2,O3,SO2                 |
| /MedioAmbiente | A03_MOLISOL_60m        |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A04_PISTASILLA_60m     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A05_POLITECNIC_60m     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A06_VIVERS_60m         |NO2,O3,SO2                 |
| /MedioAmbiente | A07_VALENCIACENTRE_60m |NO2,PM10,PM25              |
| /MedioAmbiente | A08_DR_LLUCH_60m       |NO2,PM10,PM25              |
| /MedioAmbiente | A09_CABANYAL_60m       |NO2,PM10,PM25              |
| /MedioAmbiente | A10_OLIVERETA_60m      |NO2,PM10,PM25              |
| /MedioAmbiente | A11_PATRAIX_60m        |NO2,PM10,PM25              |


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A01_AVFRANCIA_60m' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)
<details><summary>Click para ver la respuesta</summary>
{
    "method": "POST",
    "path": "/",
    "query": {},
    "client_ip": "XXXXX",
    "url": "XXXXX",
    "headers": {
      "host": "XXXXX",
      "content-length": "3560",
      "user-agent": "orion/3.7.0 libcurl/7.74.0",
      "fiware-service": "XXXXX",
      "fiware-servicepath": "/MedioAmbiente",
      "x-auth-token": "XXXXX",
      "accept": "application/json",
      "content-type": "application/json; charset=utf-8",
      "fiware-correlator": "8167df19-5722-4753-8e83-5e2a33698b80; cbnotif=5",
      "ngsiv2-attrsformat": "normalized"
    },
    "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"A01_AVFRANCIA_60m\",\"type\":\"AirQualityObserved\",\"NO2Alerta\":{\"type\":\"Number\",\"value\":\"400\",\"metadata\":{}},\"NO2Aviso\":{\"type\":\"Number\",\"value\":\"200\",\"metadata\":{}},\"NO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Nitrogeno\",\"metadata\":{}},\"NO2DescriptionPrev\":{\"type\":\"Text\",\"value\":\"Dióxido de Nitrogeno\",\"metadata\":{}},\"NO2Name\":{\"type\":\"Text\",\"value\":\"NO2\",\"metadata\":{}},\"NO2NamePrev\":{\"type\":\"Text\",\"value\":\"NO2\",\"metadata\":{}},\"NO2Preaviso\":{\"type\":\"Number\",\"value\":\"180\",\"metadata\":{}},\"NO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"NO2TypePrev\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"NO2Value\":{\"type\":\"Number\",\"value\":\"35\",\"metadata\":{}},\"NO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"NO2ValueOrigin\":{\"type\":\"Number\",\"value\":\"35.0\",\"metadata\":{}},\"NO2ValuePrev\":{\"type\":\"Number\",\"value\":\"32\",\"metadata\":{}},\"O3Description\":{\"type\":\"Text\",\"value\":\"Ozono\",\"metadata\":{}},\"O3Name\":{\"type\":\"Text\",\"value\":\"O3\",\"metadata\":{}},\"O3Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"O3Value\":{\"type\":\"Number\",\"value\":\"32\",\"metadata\":{}},\"O3ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"O3ValueOrigin\":{\"type\":\"Number\",\"value\":\"31.729166\",\"metadata\":{}},\"PM10CorrectionFactor\":{\"type\":\"Number\",\"value\":\"1.0\",\"metadata\":{}},\"PM10Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 10 micras\",\"metadata\":{}},\"PM10Name\":{\"type\":\"Text\",\"value\":\"PM10\",\"metadata\":{}},\"PM10Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM10Value\":{\"type\":\"Number\",\"value\":\"40\",\"metadata\":{}},\"PM10ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM10ValueFull\":{\"type\":\"Number\",\"value\":\"40.0\",\"metadata\":{}},\"PM10ValueOrigin\":{\"type\":\"Number\",\"value\":\"39.79305\",\"metadata\":{}},\"PM25Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 2.5 micras\",\"metadata\":{}},\"PM25Name\":{\"type\":\"Text\",\"value\":\"PM25\",\"metadata\":{}},\"PM25Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM25Value\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"PM25ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM25ValueOrigin\":{\"type\":\"Number\",\"value\":\"4.358333\",\"metadata\":{}},\"SO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Azufre\",\"metadata\":{}},\"SO2Name\":{\"type\":\"Text\",\"value\":\"SO2\",\"metadata\":{}},\"SO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"SO2Value\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"SO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"SO2ValueOrigin\":{\"type\":\"Number\",\"value\":\"4.3000007\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"AVDA.FRANCIA\",\"metadata\":{}},\"calidad_ambiental\":{\"type\":\"Text\",\"value\":\"Razonablemente Buena\",\"metadata\":{}},\"calidad_ambiental_id\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T10:00:00.000Z\",\"metadata\":{}},\"dateObservedGMT0\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T08:00:00.000Z\",\"metadata\":{}},\"gis_id\":{\"type\":\"Text\",\"value\":\"Francia\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.3426602,39.4575225],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"A01_AVFRANCIA\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}}}]}",
    "body": {
      "subscriptionId": "XXXXX",
      "data": [
        {
          "id": "A01_AVFRANCIA_60m",
          "type": "AirQualityObserved",
          "NO2Alerta": {
            "type": "Number",
            "value": "400",
            "metadata": {}
          },
          "NO2Aviso": {
            "type": "Number",
            "value": "200",
            "metadata": {}
          },
          "NO2Description": {
            "type": "Text",
            "value": "Dióxido de Nitrogeno",
            "metadata": {}
          },
          "NO2DescriptionPrev": {
            "type": "Text",
            "value": "Dióxido de Nitrogeno",
            "metadata": {}
          },
          "NO2Name": {
            "type": "Text",
            "value": "NO2",
            "metadata": {}
          },
          "NO2NamePrev": {
            "type": "Text",
            "value": "NO2",
            "metadata": {}
          },
          "NO2Preaviso": {
            "type": "Number",
            "value": "180",
            "metadata": {}
          },
          "NO2Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "NO2TypePrev": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "NO2Value": {
            "type": "Number",
            "value": "35",
            "metadata": {}
          },
          "NO2ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "NO2ValueOrigin": {
            "type": "Number",
            "value": "35.0",
            "metadata": {}
          },
          "NO2ValuePrev": {
            "type": "Number",
            "value": "32",
            "metadata": {}
          },
          "O3Description": {
            "type": "Text",
            "value": "Ozono",
            "metadata": {}
          },
          "O3Name": {
            "type": "Text",
            "value": "O3",
            "metadata": {}
          },
          "O3Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "O3Value": {
            "type": "Number",
            "value": "32",
            "metadata": {}
          },
          "O3ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "O3ValueOrigin": {
            "type": "Number",
            "value": "31.729166",
            "metadata": {}
          },
          "PM10CorrectionFactor": {
            "type": "Number",
            "value": "1.0",
            "metadata": {}
          },
          "PM10Description": {
            "type": "Text",
            "value": "Particulas en suspensión inferiores a 10 micras",
            "metadata": {}
          },
          "PM10Name": {
            "type": "Text",
            "value": "PM10",
            "metadata": {}
          },
          "PM10Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "PM10Value": {
            "type": "Number",
            "value": "40",
            "metadata": {}
          },
          "PM10ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "PM10ValueFull": {
            "type": "Number",
            "value": "40.0",
            "metadata": {}
          },
          "PM10ValueOrigin": {
            "type": "Number",
            "value": "39.79305",
            "metadata": {}
          },
          "PM25Description": {
            "type": "Text",
            "value": "Particulas en suspensión inferiores a 2.5 micras",
            "metadata": {}
          },
          "PM25Name": {
            "type": "Text",
            "value": "PM25",
            "metadata": {}
          },
          "PM25Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "PM25Value": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "PM25ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "PM25ValueOrigin": {
            "type": "Number",
            "value": "4.358333",
            "metadata": {}
          },
          "SO2Description": {
            "type": "Text",
            "value": "Dióxido de Azufre",
            "metadata": {}
          },
          "SO2Name": {
            "type": "Text",
            "value": "SO2",
            "metadata": {}
          },
          "SO2Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "SO2Value": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "SO2ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "SO2ValueOrigin": {
            "type": "Number",
            "value": "4.3000007",
            "metadata": {}
          },
          "address": {
            "type": "Text",
            "value": "AVDA.FRANCIA",
            "metadata": {}
          },
          "calidad_ambiental": {
            "type": "Text",
            "value": "Razonablemente Buena",
            "metadata": {}
          },
          "calidad_ambiental_id": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "dateObserved": {
            "type": "DateTime",
            "value": "2022-09-08T10:00:00.000Z",
            "metadata": {}
          },
          "dateObservedGMT0": {
            "type": "DateTime",
            "value": "2022-09-08T08:00:00.000Z",
            "metadata": {}
          },
          "gis_id": {
            "type": "Text",
            "value": "Francia",
            "metadata": {}
          },
          "location": {
            "type": "geo:json",
            "value": {
              "coordinates": [
                -0.3426602,
                39.4575225
              ],
              "type": "Point"
            },
            "metadata": {}
          },
          "maintenanceOwner": {
            "type": "Text",
            "value": "OCI",
            "metadata": {}
          },
          "maintenanceOwnerEmail": {
            "type": "Text",
            "value": "XXXXX",
            "metadata": {}
          },
          "operationalStatus": {
            "type": "Text",
            "value": "ok",
            "metadata": {}
          },
          "refPointOfInterest": {
            "type": "Text",
            "value": "A01_AVFRANCIA",
            "metadata": {}
          },
          "serviceOwner": {
            "type": "Text",
            "value": "OCI",
            "metadata": {}
          },
          "serviceOwnerEmail": {
            "type": "Text",
            "value": "XXXXX",
            "metadata": {}
          }
        }
      ]
    }
  }
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.
