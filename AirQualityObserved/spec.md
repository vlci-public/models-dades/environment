**Índice**  

 [[_TOC_]]  
  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `address`: La dirección postal.  
- `project`: proyecto al que pertenece esta entidad
- `description`: Una descripción de este artículo  

##### Atributos descriptivos de la entidad (OPCIONALES)
- `airQualityIndex`: Índice de calidad del aire correspondiente a la calidad del aire observada  
- `airQualityLevel`: Nivel cualitativo global de preocupación por la salud correspondiente a la calidad del aire observada  
- `dataProvider`: Una secuencia de caracteres que identifica al proveedor de la entidad de datos armonizada.
- `refPointOfInterest`: Una referencia a un punto de interés (normalmente una estación de calidad del aire) asociada a esta observación.  
- `reliability`: Fiabilidad (porcentaje, expresado en partes por uno) correspondiente a la calidad del aire observada  

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601 UTC. La fecha/hora es el final de la medición. Por defecto las mediciones son diezminutales.
- `no2`: Dióxido de nitrógeno detectado. Unidad de medida: µg/m3.
- `no`: Monóxido de nitrógeno detectado. Unidad de medida: µg/m3.
- `nox`: Otros óxidos de nitrógeno detectados. Unidad de medida: µg/m3.
- `o3`: Ozono detectado. Unidad de medida: µg/m3.
- `pm1`: Partículas de 1 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 1 µm. Unidad de medida: µg/m3.
- `pm10`: Partículas de 10 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro está aproximadamente entre 2.5 y 10 µm. Unidad de medida: µg/m3.
- `pm25`: Partículas de 2,5 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 2.5 µm. Unidad de medida: µg/m3.
- `so2`: Dióxido de azufre detectado. Unidad de medida: µg/m3.

##### Atributos para realizar cálculos / lógica de negocio
- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad         |
|----------------|--------------------|
| /MedioAmbiente | A08_DR_LLUCH_RT    |
| /MedioAmbiente | A09_CABANYAL_RT    |
| /MedioAmbiente | DipuValTurismo_XXX |


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A08_DR_LLUCH_RT' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```


Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades
