**Index**

 [[_TOC_]]  

# Entity: AirQualityObserved

[Based on Fiware AirQualityObserved entity](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Global description: **Air quality monitoring using sensors mounted on EMT buses.**

Uses the standards and best practices defined [in this link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Property List  

##### Context broker entity attributes

- `id`: Unique identifier of the entity.
- `type`: NGSI Entity type.

##### Entity descriptive attributes

- `name`: The name of this item.
- `location`: Geojson reference to the element. Can be Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon.
- `project`: Project to which this entity belongs.
- `idBus`: Identifier consisting of a positive integer. Only characters [0-9]

##### Optional entity descriptive attributes

- None.

##### Measurement attributes

- `temperatureInt`: Temperature inside the vehicle in celsius degrees.
- `temperatureExt`: Temperature outside the vehicle in celsius degrees.
- `relativeHumidityInt`: Relative humidity percentage inside the vehicle.
- `relativeHumidityExt`: Relative humidity percentage outside the vehicle.
- `PM25Int`: Particles of 2.5 micrometers or less in diameter. Amount of solid or liquid particles of dust, ash, soot, metallic particles, cement and pollen, dispersed in the atmosphere, with a diameter of approximately 2.5 µm. Unit of measurement: µg/m3.
- `LaeqInt`: Constant sound pressure level, measured in decibels (dB), equivalent to the average sound energy over a specific time interval.
- `COExt`: Carbon monoxide concentration in outdoor air, measured in micrograms per cubic meter (µg/m³).
- `NO2Ext`: Nitrogen dioxide concentration in outdoor air, measured in micrograms per cubic meter (µg/m³).
- `O3Ext`: Ozone concentration in outdoor air, measured in micrograms per cubic meter (µg/m³).
- `dateObserved`: The date and time of this observation in ISO8601 UTC format. The date/time is the end of the measurement. By default, measurements are ten-minute intervals.
- `TimeInstant`: The UTC date and time when the platform receives the data, expressed in ISO8601 format.

##### Attributes for calculations / business logic

- None

##### Attributes for GIS / Graphical entity representation

- None

##### Monitoring attributes

- `operationalStatus`: Indicates if the entity is receiving data. Possible values: ok, noData
- `inactive`: (Boolean) Possible values: true, false. True - the entity is down or inactive. Used to exclude it from status monitoring. Intended for cases where the entity is known a priori that it will never be updated and therefore processes that make use of that operationalStatus should ignore this property.
- `maintenanceOwner`: Technical responsible for this entity
- `maintenanceOwnerEmail`: Email of the technical responsible for this entity
- `serviceOwner`: Municipal service contact person for this entity
- `serviceOwnerEmail`: Email of the municipal service contact person for this entity

##### Unnecessary attributes in future entities

- None

## List of entities implementing this model

| Subservice    | Entity ID             |
|--------------------|------------------------|
| /medioambiente_emt | sensoremt6546    |

##### Entity example

```curl
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/sensoremt6546' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Attributes sent to GIS to be used in graphic representation
===========================
- None. No GIS subscription exist for this entities
