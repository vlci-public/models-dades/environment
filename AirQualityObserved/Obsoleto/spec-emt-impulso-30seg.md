**Índice**  

 [[_TOC_]]  
  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire en un lugar y momento determinados. Esta entidad es específica de la EMT y se alimenta a través de dispositivos IoT. Es una traslación directa del envío del dispositivo. Los datos se actualizan cada 30 segundos.**

Este modelo pertenece al proyecto Impulso VLCi. Se considera obsoleto, pero se mantiene con motivo de tener futuras referencias.

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `project`: Proyecto al que pertenece
- `refDevice`: Referencia del dispositivo

##### Atributos de la medición
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601, convertido al timezone Europe/Madrid
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT
- `NO2`: Dióxido de nitrógeno detectado, tal y como viene del sistema origen (sin formatear)
- `O3`: Ozono detectado, tal y como viene del sistema origen (sin formatear)
- `PM25`: Partículas de 2,5 micrómetros o menos de diámetro, tal y como viene del sistema origen (sin formatear)
- `CO`: Monóxido de carbono detectado, tal y como viene del sistema origen (sin formatear)
- `noiseLevel`: Nivel de ruido detectado, tal y como viene del sistema origen (sin formatear)
- `intTemperature`: Temperatura interna detectada, tal y como viene del sistema origen (sin formatear)
- `extTemperature`: Temperatura externa detectada, tal y como viene del sistema origen (sin formatear)
- `intRelativeHumidity`: Humedad relativa interna detectada, tal y como viene del sistema origen (sin formatear)
- `extRelativeHumidity`: Humedad relativa externa detectada, tal y como viene del sistema origen (sin formatear)

##### Atributos para realizar cálculos / lógica de negocio
- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad
- Ninguno

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades
- Ninguno

## Lista de entidades que implementan este modelo
Actualmente existen 58 entidades con una frecuencia de actualización de 30 segundos. Todas cumplen el patrón AirQualityObserved:XXX, por ejemplo:

| Subservicio    | ID Entidad             |
|--------------------|------------------------|
| /medioambiente_emt | AirQualityObserved:8MGV2DQEYC    |
| /medioambiente_emt | AirQualityObserved:PV5036C6PC    |


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/AirQualityObserved:8MGV2DQEYC' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2280",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "XXXXX",
    "fiware-servicepath": "/medioambiente_emt",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "94ba99c8-2f6c-11ed-908e-0a580a83058f; cbnotif=13",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"AirQualityObserved:WA4FARPN6L\",\"type\":\"AirQualityObserved\",\"CO\":{\"type\":\"Number\",\"value\":0.0429,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"NO2\":{\"type\":\"Number\",\"value\":38.8353,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"O3\":{\"type\":\"Number\",\"value\":117.5544,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"PM25\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DataTime\",\"value\":\"2022-09-08T13:51:28.946\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"extRelativeHumidity\":{\"type\":\"Number\",\"value\":40.3106,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"extTemperature\":{\"type\":\"Number\",\"value\":37.9335,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"intRelativeHumidity\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"intTemperature\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"location\":{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-0.39153668,39.479088]},\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"noiseLevel\":{\"type\":\"Number\",\"value\":49.805,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"project\":{\"type\":\"Text\",\"value\":\"impulstoVLCI-red.es\",\"metadata\":{}},\"refDevice\":{\"type\":\"Relationship\",\"value\":\"Device:WA4FARPN6L\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "AirQualityObserved:WA4FARPN6L",
        "type": "AirQualityObserved",
        "CO": {
          "type": "Number",
          "value": 0.0429,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "NO2": {
          "type": "Number",
          "value": 38.8353,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "O3": {
          "type": "Number",
          "value": 117.5544,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "PM25": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-08T11:51:31.643Z",
          "metadata": {}
        },
        "dateObserved": {
          "type": "DataTime",
          "value": "2022-09-08T13:51:28.946",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "extRelativeHumidity": {
          "type": "Number",
          "value": 40.3106,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "extTemperature": {
          "type": "Number",
          "value": 37.9335,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "intRelativeHumidity": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "intTemperature": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [
              -0.39153668,
              39.479088
            ]
          },
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "noiseLevel": {
          "type": "Number",
          "value": 49.805,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "project": {
          "type": "Text",
          "value": "impulstoVLCI-red.es",
          "metadata": {}
        },
        "refDevice": {
          "type": "Relationship",
          "value": "Device:WA4FARPN6L",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- Ninguno. No existe suscripción al GIS para estas entidades
