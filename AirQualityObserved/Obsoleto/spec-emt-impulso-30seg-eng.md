**Índice**  

 [[_TOC_]]  
  

# Entity: AirQualityObserved

[Based upon the Fiware entity AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

General description: **An observation of air quality conditions at a certain place and time. This entity is an specification from EMT and feeds of IoT devices. This entity is a direct translation of the data received from said devices. The data gets updated every 30 seconds.**

This model belongs to the Impulso VLCi project. It is considered obsolete but is maintained for future reference purposes.

Uses the best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Properties List

##### Context broker entity Attributes

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Entity description attributes

- `location`: Geojson reference, it can be a Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `project`: Project to which this entity belongs to
- `refDevice`: A reference to the device which captured this observation.

##### Atributos de la medición

- `dateObserved`: The date and time of the measurement in ISO8601 format, converted to the Europe/Madrid timezone
- `TimeInstant`: The date and time where the data was sent from the IoT agent
- `NO2`: Nitrogen dioxide measured, exactly as in the origin system (With no format applied)
- `O3`: Ozone measured, exactly as received from the origin system (With no format applied)
- `PM25`: Particulate matter 2,5 micrometers or fewer in diameter, exactly as received from the origin system (With no format applied)
- `CO`: Carbon monoxude measured, exactly as received from the origin system (With no format applied)
- `noiseLevel`: Noise level measured, exactly as received from the origin system (With no format applied)
- `intTemperature`: Internal temperature measured, exactly as received from the origin system (With no format applied)
- `extTemperature`: External temperature measured, exactly as received from the origin system (With no format applied)
- `intRelativeHumidity`: Internal relative humidity measured, exactly as received from the origin system (With no format applied)
- `extRelativeHumidity`: External humidity measured, exactly as received from the origin system (With no format applied)

##### Attributes to be used in calculations / Business logic

- None

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Possible values: ok, noData
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity list

At the moment 58 entities exist with an update rate of 30 seconds. All of which follows the naming pattern AirQualityObserved:XXX, for instance:

| Sub servide        | Entity ID                     |
| ------------------ | ----------------------------- |
| /medioambiente_emt | AirQualityObserved:8MGV2DQEYC |
| /medioambiente_emt | AirQualityObserved:PV5036C6PC |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/AirQualityObserved:8MGV2DQEYC' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2280",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "XXXXX",
    "fiware-servicepath": "/medioambiente_emt",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "94ba99c8-2f6c-11ed-908e-0a580a83058f; cbnotif=13",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"AirQualityObserved:WA4FARPN6L\",\"type\":\"AirQualityObserved\",\"CO\":{\"type\":\"Number\",\"value\":0.0429,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"NO2\":{\"type\":\"Number\",\"value\":38.8353,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"O3\":{\"type\":\"Number\",\"value\":117.5544,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"PM25\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DataTime\",\"value\":\"2022-09-08T13:51:28.946\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"extRelativeHumidity\":{\"type\":\"Number\",\"value\":40.3106,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"extTemperature\":{\"type\":\"Number\",\"value\":37.9335,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"intRelativeHumidity\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"intTemperature\":{\"type\":\"Number\",\"value\":0,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"location\":{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-0.39153668,39.479088]},\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"noiseLevel\":{\"type\":\"Number\",\"value\":49.805,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T11:51:31.643Z\"}}},\"project\":{\"type\":\"Text\",\"value\":\"impulstoVLCI-red.es\",\"metadata\":{}},\"refDevice\":{\"type\":\"Relationship\",\"value\":\"Device:WA4FARPN6L\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "AirQualityObserved:WA4FARPN6L",
        "type": "AirQualityObserved",
        "CO": {
          "type": "Number",
          "value": 0.0429,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "NO2": {
          "type": "Number",
          "value": 38.8353,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "O3": {
          "type": "Number",
          "value": 117.5544,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "PM25": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-08T11:51:31.643Z",
          "metadata": {}
        },
        "dateObserved": {
          "type": "DataTime",
          "value": "2022-09-08T13:51:28.946",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "extRelativeHumidity": {
          "type": "Number",
          "value": 40.3106,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "extTemperature": {
          "type": "Number",
          "value": 37.9335,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "intRelativeHumidity": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "intTemperature": {
          "type": "Number",
          "value": 0,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [
              -0.39153668,
              39.479088
            ]
          },
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "noiseLevel": {
          "type": "Number",
          "value": 49.805,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-08T11:51:31.643Z"
            }
          }
        },
        "project": {
          "type": "Text",
          "value": "impulstoVLCI-red.es",
          "metadata": {}
        },
        "refDevice": {
          "type": "Relationship",
          "value": "Device:WA4FARPN6L",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

# Attributes sent to GIS to be used in graphic representation

- None. No GIS subscription exist for this entities
