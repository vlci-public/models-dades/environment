**Índice**  

 [[_TOC_]]  
  

# Entity: AirQualityObserved

[Based upon the Fiware entity AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

General description: **An observation of the air quality conditions at a certain time in a certain place, this entity is an specification from [BSG](https://www.bsg.es/en/index) and receives its date from an ETL that calculates the median values using the 10 minute samples provided by BSG.**

Uses the best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

**OF NOTE: Not all measuring stations provide with data for all pollutants, the following table specify which measurements are provided by each station.**

## Properties List

##### Context broker entity Attributes

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Entity description attributes

- `location`: Geojson reference, it can be a Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `refPointOfInterest`: Entity ID, refers to a PointOfInterest entity type, wich describes the station
- `address`: The name of the street in which the station is placed
- `calidad_ambiental`: Overall qualitative level of health concern corresponding to the air quality observed, equivalent to airQualityLevel from FIWARE posible values: Muy bueno (excelent), bueno (good), etc
- `calidad_ambiental_id`: Air quality index is a number (range from 1 to 5) used to report the quality of the air on any given day. Equivalent to airQualityIndex from FIWARE

##### (OPTIONAL) Entity description attributes

- None

##### Measurement attributes

- `dateObserved`: The date and time of the measurement in ISO8601 format, converted to the Europe/Madrid timezone
- `dateObservedGMT0`: The date and time of this observation in ISO8601 UTCformat. Model: [https://schema.org/Text](https://schema.org/Text)
- `NO2ValueOrigin`: Nitrogen dioxide measured, exactly as in the origin system (With no format applied)
- `NO2Value`: Nitrogen dioxide measured, formatted into an integer
- `NO2ValueFlag`: Posible values: V or F. V means valid measurement, any other value means not valid measurement
- `O3ValueOrigin`: Ozone measured, exactly as received from the origin system (With no format applied)
- `O3Value`: Ozone measured, formatted into an integer
- `O3ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10Value`: Particulate matter 10 micrometers or fewer in diameter, formatted into an integer
- `PM10ValueFull`: Particulate matter 10 micrometers or fewer in diameter with a correction factor applied on ValueOrigin, not rounded
- `PM10ValueOrigin`: Particulate matter 10 micrometers or fewer in diameter, exactly as in the origin system (With no format applied)
- `PM25Value`: Particulate matter 2,5 micrometers or fewer in diameter, formatted into an integer
- `PM25ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM25ValueOrigin`: Particulate matter 2,5 micrometers or fewer in diameter, exactly as received from the origin system (With no format applied)
- `SO2Value`: Sulphur dioxide measured, formatted into an integer
- `SO2ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `SO2ValueOrigin`: Sulphur dioxide measured, exactly as received from the origin system (With no format applied)

##### Measurement Description attributes

- `NO2Description`: Measurement description
- `NO2Name`: Pollutant name (Ex: NO2)
- `NO2Type`: Measurement unit (µg/m3)
- `O3Description`: Measurement description
- `O3Name`: Pollutant name (Ex: NO2)
- `O3Type`: Measurement unit (µg/m3)
- `PM10CorrectionFactor`: Correction Factor to be applied to the PM10 measurement
- `PM10Description`: Measurement description
- `PM10Name`: Pollutant name (Ex: NO2)
- `PM10Type`: Measurement unit (µg/m3)
- `PM25Description`: Measurement description
- `PM25Name`: Pollutant name (Ex: NO2)
- `PM25Type`: Measurement unit (µg/m3)
- `SO2Description`: Measurement description
- `SO2Name`: Pollutant name (Ex: NO2)
- `SO2Type`: Measurement unit (µg/m3)

##### Atributos para realizar cálculos / lógica de negocio

- `NO2Alerta`: Default value: 400. This value sets the limit NO2 measurement before an alert is triggered. Used to send a third level alert on an email
- `NO2Aviso`: Default value: 200. This value sets the limit NO2 measurement before an alert is triggered. Used to send a second level alert on an email
- `NO2Preaviso`: Default value: 180. This value sets the limit NO2 measurement before an alert is triggered. Used to send a first level alert on an email
- `NO2DescriptionPrev`: Last measuremet value of the attribute NO2Description, used for the alerts
- `NO2NamePrev`: Last measuremet value of the attribute NO2Name, used for the alerts
- `NO2TypePrev`: Last measuremet value of the attribute NO2Type, used for the alerts
- `NO2ValuePrev`: Last measuremet value of the attribute NO2Value, used for the alerts
- `gis_id`: Identifier name for the municipal gis postgres schema. Possible values: Francia, Boulevar Sur, Molí del Sol, Pista de Silla, Universidad Politécnica, Viveros, Centro.
- `calculatedTimestamp`: Defines the moment in which the entity has been updated with all the calculated data.

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Possible values: ok, noData
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity list and Pollutants measured

| Sub service    | Entity ID              | Pollutants measured  |
| -------------- | ---------------------- | -------------------- |
| /MedioAmbiente | A01_AVFRANCIA_60m      | NO2,O3,PM10,PM25,SO2 |
| /MedioAmbiente | A02_BULEVARDSUD_60m    | NO2,O3,SO2           |
| /MedioAmbiente | A03_MOLISOL_60m        | NO2,O3,PM10,PM25,SO2 |
| /MedioAmbiente | A04_PISTASILLA_60m     | NO2,O3,PM10,PM25,SO2 |
| /MedioAmbiente | A05_POLITECNIC_60m     | NO2,O3,PM10,PM25,SO2 |
| /MedioAmbiente | A06_VIVERS_60m         | NO2,O3,SO2           |
| /MedioAmbiente | A07_VALENCIACENTRE_60m | NO2,PM10,PM25        |
| /MedioAmbiente | A08_DR_LLUCH_60m       | NO2,PM10,PM25        |
| /MedioAmbiente | A09_CABANYAL_60m       | NO2,PM10,PM25        |
| /MedioAmbiente | A10_OLIVERETA_60m      | NO2,PM10,PM25        |
| /MedioAmbiente | A11_PATRAIX_60m        | NO2,PM10,PM25        |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A01_AVFRANCIA_60m' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>
{
    "method": "POST",
    "path": "/",
    "query": {},
    "client_ip": "XXXXX",
    "url": "XXXXX",
    "headers": {
      "host": "XXXXX",
      "content-length": "3560",
      "user-agent": "orion/3.7.0 libcurl/7.74.0",
      "fiware-service": "XXXXX",
      "fiware-servicepath": "/MedioAmbiente",
      "x-auth-token": "XXXXX",
      "accept": "application/json",
      "content-type": "application/json; charset=utf-8",
      "fiware-correlator": "8167df19-5722-4753-8e83-5e2a33698b80; cbnotif=5",
      "ngsiv2-attrsformat": "normalized"
    },
    "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"A01_AVFRANCIA_60m\",\"type\":\"AirQualityObserved\",\"NO2Alerta\":{\"type\":\"Number\",\"value\":\"400\",\"metadata\":{}},\"NO2Aviso\":{\"type\":\"Number\",\"value\":\"200\",\"metadata\":{}},\"NO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Nitrogeno\",\"metadata\":{}},\"NO2DescriptionPrev\":{\"type\":\"Text\",\"value\":\"Dióxido de Nitrogeno\",\"metadata\":{}},\"NO2Name\":{\"type\":\"Text\",\"value\":\"NO2\",\"metadata\":{}},\"NO2NamePrev\":{\"type\":\"Text\",\"value\":\"NO2\",\"metadata\":{}},\"NO2Preaviso\":{\"type\":\"Number\",\"value\":\"180\",\"metadata\":{}},\"NO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"NO2TypePrev\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"NO2Value\":{\"type\":\"Number\",\"value\":\"35\",\"metadata\":{}},\"NO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"NO2ValueOrigin\":{\"type\":\"Number\",\"value\":\"35.0\",\"metadata\":{}},\"NO2ValuePrev\":{\"type\":\"Number\",\"value\":\"32\",\"metadata\":{}},\"O3Description\":{\"type\":\"Text\",\"value\":\"Ozono\",\"metadata\":{}},\"O3Name\":{\"type\":\"Text\",\"value\":\"O3\",\"metadata\":{}},\"O3Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"O3Value\":{\"type\":\"Number\",\"value\":\"32\",\"metadata\":{}},\"O3ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"O3ValueOrigin\":{\"type\":\"Number\",\"value\":\"31.729166\",\"metadata\":{}},\"PM10CorrectionFactor\":{\"type\":\"Number\",\"value\":\"1.0\",\"metadata\":{}},\"PM10Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 10 micras\",\"metadata\":{}},\"PM10Name\":{\"type\":\"Text\",\"value\":\"PM10\",\"metadata\":{}},\"PM10Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM10Value\":{\"type\":\"Number\",\"value\":\"40\",\"metadata\":{}},\"PM10ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM10ValueFull\":{\"type\":\"Number\",\"value\":\"40.0\",\"metadata\":{}},\"PM10ValueOrigin\":{\"type\":\"Number\",\"value\":\"39.79305\",\"metadata\":{}},\"PM25Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 2.5 micras\",\"metadata\":{}},\"PM25Name\":{\"type\":\"Text\",\"value\":\"PM25\",\"metadata\":{}},\"PM25Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM25Value\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"PM25ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM25ValueOrigin\":{\"type\":\"Number\",\"value\":\"4.358333\",\"metadata\":{}},\"SO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Azufre\",\"metadata\":{}},\"SO2Name\":{\"type\":\"Text\",\"value\":\"SO2\",\"metadata\":{}},\"SO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"SO2Value\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"SO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"SO2ValueOrigin\":{\"type\":\"Number\",\"value\":\"4.3000007\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"AVDA.FRANCIA\",\"metadata\":{}},\"calidad_ambiental\":{\"type\":\"Text\",\"value\":\"Razonablemente Buena\",\"metadata\":{}},\"calidad_ambiental_id\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T10:00:00.000Z\",\"metadata\":{}},\"dateObservedGMT0\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T08:00:00.000Z\",\"metadata\":{}},\"gis_id\":{\"type\":\"Text\",\"value\":\"Francia\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.3426602,39.4575225],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"A01_AVFRANCIA\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}}}]}",
    "body": {
      "subscriptionId": "XXXXX",
      "data": [
        {
          "id": "A01_AVFRANCIA_60m",
          "type": "AirQualityObserved",
          "NO2Alerta": {
            "type": "Number",
            "value": "400",
            "metadata": {}
          },
          "NO2Aviso": {
            "type": "Number",
            "value": "200",
            "metadata": {}
          },
          "NO2Description": {
            "type": "Text",
            "value": "Dióxido de Nitrogeno",
            "metadata": {}
          },
          "NO2DescriptionPrev": {
            "type": "Text",
            "value": "Dióxido de Nitrogeno",
            "metadata": {}
          },
          "NO2Name": {
            "type": "Text",
            "value": "NO2",
            "metadata": {}
          },
          "NO2NamePrev": {
            "type": "Text",
            "value": "NO2",
            "metadata": {}
          },
          "NO2Preaviso": {
            "type": "Number",
            "value": "180",
            "metadata": {}
          },
          "NO2Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "NO2TypePrev": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "NO2Value": {
            "type": "Number",
            "value": "35",
            "metadata": {}
          },
          "NO2ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "NO2ValueOrigin": {
            "type": "Number",
            "value": "35.0",
            "metadata": {}
          },
          "NO2ValuePrev": {
            "type": "Number",
            "value": "32",
            "metadata": {}
          },
          "O3Description": {
            "type": "Text",
            "value": "Ozono",
            "metadata": {}
          },
          "O3Name": {
            "type": "Text",
            "value": "O3",
            "metadata": {}
          },
          "O3Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "O3Value": {
            "type": "Number",
            "value": "32",
            "metadata": {}
          },
          "O3ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "O3ValueOrigin": {
            "type": "Number",
            "value": "31.729166",
            "metadata": {}
          },
          "PM10CorrectionFactor": {
            "type": "Number",
            "value": "1.0",
            "metadata": {}
          },
          "PM10Description": {
            "type": "Text",
            "value": "Particulas en suspensión inferiores a 10 micras",
            "metadata": {}
          },
          "PM10Name": {
            "type": "Text",
            "value": "PM10",
            "metadata": {}
          },
          "PM10Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "PM10Value": {
            "type": "Number",
            "value": "40",
            "metadata": {}
          },
          "PM10ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "PM10ValueFull": {
            "type": "Number",
            "value": "40.0",
            "metadata": {}
          },
          "PM10ValueOrigin": {
            "type": "Number",
            "value": "39.79305",
            "metadata": {}
          },
          "PM25Description": {
            "type": "Text",
            "value": "Particulas en suspensión inferiores a 2.5 micras",
            "metadata": {}
          },
          "PM25Name": {
            "type": "Text",
            "value": "PM25",
            "metadata": {}
          },
          "PM25Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "PM25Value": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "PM25ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "PM25ValueOrigin": {
            "type": "Number",
            "value": "4.358333",
            "metadata": {}
          },
          "SO2Description": {
            "type": "Text",
            "value": "Dióxido de Azufre",
            "metadata": {}
          },
          "SO2Name": {
            "type": "Text",
            "value": "SO2",
            "metadata": {}
          },
          "SO2Type": {
            "type": "Text",
            "value": "µg/m3",
            "metadata": {}
          },
          "SO2Value": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "SO2ValueFlag": {
            "type": "Text",
            "value": "V",
            "metadata": {}
          },
          "SO2ValueOrigin": {
            "type": "Number",
            "value": "4.3000007",
            "metadata": {}
          },
          "address": {
            "type": "Text",
            "value": "AVDA.FRANCIA",
            "metadata": {}
          },
          "calidad_ambiental": {
            "type": "Text",
            "value": "Razonablemente Buena",
            "metadata": {}
          },
          "calidad_ambiental_id": {
            "type": "Number",
            "value": "4",
            "metadata": {}
          },
          "dateObserved": {
            "type": "DateTime",
            "value": "2022-09-08T10:00:00.000Z",
            "metadata": {}
          },
          "dateObservedGMT0": {
            "type": "DateTime",
            "value": "2022-09-08T08:00:00.000Z",
            "metadata": {}
          },
          "gis_id": {
            "type": "Text",
            "value": "Francia",
            "metadata": {}
          },
          "location": {
            "type": "geo:json",
            "value": {
              "coordinates": [
                -0.3426602,
                39.4575225
              ],
              "type": "Point"
            },
            "metadata": {}
          },
          "maintenanceOwner": {
            "type": "Text",
            "value": "OCI",
            "metadata": {}
          },
          "maintenanceOwnerEmail": {
            "type": "Text",
            "value": "XXXXX",
            "metadata": {}
          },
          "operationalStatus": {
            "type": "Text",
            "value": "ok",
            "metadata": {}
          },
          "refPointOfInterest": {
            "type": "Text",
            "value": "A01_AVFRANCIA",
            "metadata": {}
          },
          "serviceOwner": {
            "type": "Text",
            "value": "OCI",
            "metadata": {}
          },
          "serviceOwnerEmail": {
            "type": "Text",
            "value": "XXXXX",
            "metadata": {}
          }
        }
      ]
    }
  }
</details>

# Attributes sent to GIS to be used in graphic representation

===========================

- El GIS se actualiza mediante una ETL que accede al Postgres.
