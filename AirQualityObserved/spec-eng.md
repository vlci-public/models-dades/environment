**Índice**  

 [[_TOC_]]  
  

# Entity: AirQualityObserved

[Based upon the Fiware entity AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

General description: **An observation of air quality conditions at a certain place and time.**

Uses the best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Properties List

##### Atributos de la entidad context broker

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Atributos descriptivos de la entidad

- `name`: The name of this item.
- `location`: Geojson reference, it can be a Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `address`: Postal address.
- `project`: Project to which this entity belongs to
- `description`: A description of this item

##### Atributos descriptivos de la entidad (OPCIONALES)

- `airQualityIndex`: Air quality index is a number used to report the quality of the air on any given day
- `airQualityLevel`: Overall qualitative level of health concern corresponding to the air quality observed
- `dataProvider`: A sequence of characters identifying the provider of the harmonised data entity
- `refPointOfInterest`: A Reference to a point of interest (usually an air quality station) asosiated to this observation
- `reliability`: Reliability (percentage, expressed in parts per one) corresponding to the air quality observed

##### Atributos de la medición

- `dateObserved`: The date and time of this observation in ISO8601 UTCformat
- `no2`: Dióxido de nitrógeno detectado
- `no`: Nitrogen dioxide measured
- `nox`: Other Nitrogen oxides detected
- `o3`: Ozone measured
- `pm1`: Particulate matter 1 micrometers or less in diameter
- `pm10`: Particulate matter 10 micrometers or less in diameter
- `pm25`: Particulate matter 2.5 micrometers or less in diameter
- `so2`: Sulfur dioxide measured

##### Attributes to be used in calculations / Business logic

- None

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Signals if the entity is receiving data. Possible values: ok, noData
- `inactive`: (Boolean) Possible values: true, false. If true - operationalStatus should be ignored.
  Designed for cases in which the entity will never be updated so the processes that use operationalStatus ignore the attribute
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity List

| Sub service    | Entity ID       |
| -------------- | --------------- |
| /MedioAmbiente | A08_DR_LLUCH_RT |
| /MedioAmbiente | A09_CABANYAL_RT |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A08_DR_LLUCH_RT' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

<details><summary>Click para ver la respuesta</summary>

HEADERS:

Host: XXXXX
X-Amzn-Trace-Id: Root=XXXXX
Content-Length: 2491
user-agent: orion/3.7.0 libcurl/7.74.0
fiware-service: sc_vlci
fiware-servicepath: /MedioAmbiente
accept: application/json
content-type: application/json; charset=utf-8
fiware-correlator: b15130dc-7162-11ed-b585-0a580a81023a; cbnotif=1
ngsiv2-attrsformat: normalized

BODY:

"root":

"subscriptionId": "6388812f1e5e7d0ef326a432"

"data":

0:

"id": "A08_DR_LLUCH_RT"

"type": "AirQualityObserved"

"TimeInstant":

"type": "DateTime"

"value": "2022-12-01T10:27:01.393Z"

"metadata": 0 properties

"address":

"type": "Text"

"value": "Calle Dr. Lluch, 48"

"metadata": 0 properties

"airQualityIndex":

"type": "Text"

"value": ""

"metadata": 0 properties

"airQualityLevel":

"type": "Text"

"value": ""

"metadata": 0 properties

"dataProvider":

"type": "Text"

"value": "BSG"

"metadata": 0 properties

"dateObserved":

"type": "DateTime"

"value": "2022-12-01T10:27:01.000Z"

"metadata": 1 property

"dateObservedFrom":

"type": "DateTime"

"value": "2022-12-01T10:27:01.000Z"

"metadata": 1 property

"description":

"type": "Text"

"value": "EstaciÃ³n de mediciÃ³n de calidad del aire en el C.M.A.P.M. Canyamelar-Cabanyal"

"metadata": 0 properties

"location":

"type": "geo:json"

"value": 2 properties

"metadata": 0 properties

"maintenanceOwner":

"type": "Text"

"value": "OCI"

"metadata": 0 properties

"maintenanceOwnerEmail":

"type": "Text"

"value": "integracionvlci@valencia.es"

"metadata": 0 properties

"name":

"type": "Text"

"value": "DR.LLUCH"

"metadata": 0 properties

"no":

"type": "Number"

"value": 1

"metadata": 1 property

"no2":

"type": "Number"

"value": 16

"metadata": 1 property

"nox":

"type": "Number"

"value": 18

"metadata": 1 property

"o3":

"type": "Number"

"value": ""

"metadata": 0 properties

"operationalStatus":

"type": "Text"

"value": "ok"

"metadata": 0 properties

"inactive":

"type": "Boolean"

"value": false

"metadata": 0 properties

"pm1":

"type": "Number"

"value": 4.6

"metadata": 1 property

"pm10":

"type": "Number"

"value": 112.6

"metadata": 1 property

"pm25":

"type": "Number"

"value": 27.1

"metadata": 1 property

"project":

"type": "Text"

"value": "EDUSI-Cabanyal"

"metadata": 0 properties

"refPointOfInterest":

"type": "Text"

"value": "A08_DR_LLUCH"

"metadata": 0 properties

"reliability":

"type": "Number"

"value": 1

"metadata": 0 properties

"serviceOwner":

"type": "Text"

"value": "OCI"

"metadata": 0 properties

"serviceOwnerEmail":

"type": "Text"

"value": "integracionvlci@valencia.es"

"metadata": 0 properties

"so2":

"type": "Number"

"value": ""

"metadata": 0 properties

</details>

# Attributes sent to GIS to be used in graphic representation

- None. No GIS subscription exist for this entities
