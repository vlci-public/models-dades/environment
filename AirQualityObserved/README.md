Especificación, datos realtime
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec.md) que se tiene . Contiene las entidades del proyecto EDUSI-Cabanyal, de BSG. Son entidades que reciben datos en modo realtime, directamente desde la estación. Los llamamos "datos sucios" porque no tienen ningún tipo de validación.

Especificación: datos validados via webservice
===
Entidades de BSG via webservice, con datos validados 10 minutales [Especificación BSG 10 minutal](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-bsg-10min.md)


Especificación: datos calculados por una ETL, a partir de los datos validados via webservice
===
Entidades con datos calculados por parte de una ETL, con periodicidad horaria [Especificación BSG 60 minutal](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-bsg-60min.md)

Entidades con datos calculados por parte de una ETL, con periodicidad diaria [Especificación BSG diaria](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-bsg-24hour.md)


Especificaciones para la estación de contaminación de Kunak dentro del proyecto de ecoterrazas sostenibles
===
Entidades de los sensores de Kunak vía agentes IoT, con datos cincominutales [Especificación Kunak 5 minutal](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-kunak-5min.md)

Entidades de los sensores de Kunak vía agentes IoT, con datos horarios de promedios móviles durante los últimos 60 minutos [Especificación Kunak 60 minutos](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-kunak-60min.md)

Entidades de los sensores de Kunak vía agentes IoT, con datos horarios de promedios móviles durante las últimas 8 horas [Especificación Kunak 8 horas](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-kunak-8hour.md)

Entidades de los sensores de Kunak vía agentes IoT, con datos horarios de promedios móviles durante las últimas 24 horas [Especificación Kunak 24 horas](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-kunak-24hour.md)

Especificación para los autobuses de la EMT
===
Entidades de la EMT vía agentes IoT, con datos validados cada 30 segundos [Especificación EMT](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/AirQualityObserved/spec-emt.md)

No reusar en el futuro, debería crearse una entidad específica para los autobuses que midan distintas observaciones medioambientales y no usar la entidad AirQualityObserved como se ha hecho en este caso.
