**Índice**  

 [[_TOC_]]  
  

# Entity: AirQualityObserved

[Based upon the Fiware entity AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

General description: **An observation of the air quality conditions at a certain time in a certain place, this entity is an specification from [BSG](https://www.bsg.es/en/index) and feeds of the hourly data that in time is the median of the 10 minutes data.**

Uses the best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

**OF NOTE: Not all measuring stations provide with data for all pollutants, the following table specify which measurements are provided by each station.**

## Properties List

##### Context broker entity Attributes

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Entity description attributes

- `location`: Geojson reference, it can be a Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `refPointOfInterest`: Entity ID, refers to a PointOfInterest entity type, wich describes the station

##### (OPTIONAL) Entity description attributes

- None

##### Measurement attributes

- `dateObserved`: The date and time of the measurement in ISO8601 format, converted to the Europe/Madrid timezone
- `dateObservedGMT0`: The date and time of this observation in ISO8601 UTCformat.
- `NO2Value`: Nitrogen dioxide measured, formatted into an integer
- `NO2ValueFlag`: Posible values: V or F. V means valid measurement, any other value means not valid measurement
- `O3Value`: Ozone measured, formatted into an integer
- `O3ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10Value`: Particulate matter 10 micrometers or fewer in diameter, formatted into an integer
- `PM25Value`: Particulate matter 2,5 micrometers or fewer in diameter, formatted into an integer
- `PM25ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `SO2Value`: Sulfur dioxide measured, formatted into an integer
- `SO2ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `NOValue`: Nitrogen monoxide measured, formatted into an integer
- `NOValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `NOXValue`: Nitric monoxide measured, formatted into an integer
- `NOXValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM1Value`: Particulate matter 1 micrometer or less in diameter, formatted into an integer
- `PM1ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement

##### Measurement Description attributes

- `NO2Description`: Measurement description
- `NO2Name`: Pollutant name (Ex: NO2)
- `NO2Type`: Measurement unit (µg/m3)
- `O3Description`: Measurement description
- `O3Name`: Pollutant name (Ex: NO2)
- `O3Type`: Measurement unit (µg/m3)
- `PM10Description`: Measurement description
- `PM10Name`: Pollutant name (Ex: NO2)
- `PM10Type`: Measurement unit (µg/m3)
- `PM25Description`: Measurement description
- `PM25Name`: Pollutant name (Ex: NO2)
- `PM25Type`: Measurement unit (µg/m3)
- `SO2Description`: Measurement description
- `SO2Name`: Pollutant name (Ex: NO2)
- `SO2Type`: Measurement unit (µg/m3)
- `NODescription`: Measurement description
- `NOName`: Pollutant name (Ex: NO2)
- `NOType`: Measurement unit (µg/m3)
- `NOXDescription`: Measurement description
- `NOXName`: Pollutant name (Ex: NO2)
- `NOXType`: Measurement unit (µg/m3)
- `PM1Description`: Measurement description
- `PM1Name`: Pollutant name (Ex: NO2)
- `PM1Type`: Measurement unit (µg/m3)

##### Attributes to be used in calculations / Business logic

- `PM10Aviso`: Default value: 80. This value sets the limit NO2 measurement before an alert is triggered. Used to send a second level alert on an email
- `PM10Preaviso`: Default value: 50. This value sets the limit NO2 measurement before an alert is triggered. Used to send a first level alert on an email
- `PM10DescriptionPrev`: Last measuremet value of the attribute PM10Description, used for the alerts
- `PM10NamePrev`: Last measuremet value of the attribute PM10Name, used for the alerts
- `PM10TypePrev`: Last measuremet value of the attribute PM10Type, used for the alerts
- `PM10ValuePrev`: Last measuremet value of the attribute PM10Value, used for the alerts

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Possible values: ok, noData
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity list

| Subservicio    | ID Entidad                | Contaminantes Disponibles |
|----------------|---------------------------|---------------------------|
| /MedioAmbiente | A01_AVFRANCIA_24h         |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A02_BULEVARDSUD_24h       |NO2,O3,SO2                 |
| /MedioAmbiente | A03_MOLISOL_24h           |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A04_PISTASILLA_24h        |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A05_POLITECNIC_24h        |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A06_VIVERS_24h            |NO2,O3,SO2                 |
| /MedioAmbiente | A07_VALENCIACENTRE_24h    |NO2,PM10,PM25              |
| /MedioAmbiente | A08_DR_LLUCH_24h          |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A09_CABANYAL_24h          |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A10_OLIVERETA_24h         |NO2,PM10,PM25,NO           |
| /MedioAmbiente | A11_PATRAIX_24h           |NO2,PM10,PM25,NO,NOX       |
| /MedioAmbiente | A01_AVFRANCIA_24h_vm      |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A02_BULEVARDSUD_24h_vm    |NO2,O3,SO2                 |
| /MedioAmbiente | A03_MOLISOL_24h_vm        |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A04_PISTASILLA_24h_vm     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A05_POLITECNIC_24h_vm     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A06_VIVERS_24h_vm         |NO2,O3,SO2                 |
| /MedioAmbiente | A07_VALENCIACENTRE_24h_vm |NO2,PM10,PM25              |
| /MedioAmbiente | A08_DR_LLUCH_24h_vm       |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A09_CABANYAL_24h_vm       |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A10_OLIVERETA_24h_vm      |NO2,PM10,PM25,NO           |
| /MedioAmbiente | A11_PATRAIX_24h_vm        |NO2,PM10,PM25,NO,NOX       |
| /MedioAmbiente | A01_AVFRANCIA_24h_va      |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A02_BULEVARDSUD_24h_va    |NO2,O3,SO2                 |
| /MedioAmbiente | A03_MOLISOL_24h_va        |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A04_PISTASILLA_24h_va     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A05_POLITECNIC_24h_va     |NO2,O3,PM10,PM25,SO2       |
| /MedioAmbiente | A06_VIVERS_24h_va         |NO2,O3,SO2                 |
| /MedioAmbiente | A07_VALENCIACENTRE_24h_va |NO2,PM10,PM25              |
| /MedioAmbiente | A08_DR_LLUCH_24h_va       |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A09_CABANYAL_24h_va       |NO2,PM10,PM25,NO,NOX,PM1   |
| /MedioAmbiente | A10_OLIVERETA_24h_va      |NO2,PM10,PM25,NO           |
| /MedioAmbiente | A11_PATRAIX_24h_va        |NO2,PM10,PM25,NO,NOX       |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A01_AVFRANCIA_24h' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "3141",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/MedioAmbiente",
    "x-auth-token": "XXXXX",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "710f37d4-5a89-493b-95ff-6d8576d5295c; cbnotif=5",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"A01_AVFRANCIA_24h\",\"type\":\"AirQualityObserved\",\"NO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Nitrogeno\",\"metadata\":{}},\"NO2Name\":{\"type\":\"Text\",\"value\":\"NO2\",\"metadata\":{}},\"NO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"NO2Value\":{\"type\":\"Number\",\"value\":\"14\",\"metadata\":{}},\"NO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"O3Description\":{\"type\":\"Text\",\"value\":\"Ozono\",\"metadata\":{}},\"O3Name\":{\"type\":\"Text\",\"value\":\"O3\",\"metadata\":{}},\"O3Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"O3Value\":{\"type\":\"Number\",\"value\":\"57\",\"metadata\":{}},\"O3ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM10Aviso\":{\"type\":\"Number\",\"value\":\"80\",\"metadata\":{}},\"PM10CorrectionFactor\":{\"type\":\"Number\",\"value\":\"1.0\",\"metadata\":{}},\"PM10CorrectionFactorPrev\":{\"type\":\"Number\",\"value\":\"1.0\",\"metadata\":{}},\"PM10Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 10 micras\",\"metadata\":{}},\"PM10DescriptionPrev\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 10 micras\",\"metadata\":{}},\"PM10Name\":{\"type\":\"Text\",\"value\":\"PM10\",\"metadata\":{}},\"PM10NamePrev\":{\"type\":\"Text\",\"value\":\"PM10\",\"metadata\":{}},\"PM10Preaviso\":{\"type\":\"Number\",\"value\":\"50\",\"metadata\":{}},\"PM10Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM10TypePrev\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM10Value\":{\"type\":\"Number\",\"value\":\"32\",\"metadata\":{}},\"PM10ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"PM10ValueFull\":{\"type\":\"Number\",\"value\":\"32.0\",\"metadata\":{}},\"PM10ValueFullPrev\":{\"type\":\"Number\",\"value\":\"42.0\",\"metadata\":{}},\"PM10ValueOrigin\":{\"type\":\"Number\",\"value\":\"31.791666666666668\",\"metadata\":{}},\"PM10ValueOriginPrev\":{\"type\":\"Number\",\"value\":\"42.333333333333336\",\"metadata\":{}},\"PM10ValuePrev\":{\"type\":\"Number\",\"value\":\"42\",\"metadata\":{}},\"PM25Description\":{\"type\":\"Text\",\"value\":\"Particulas en suspensión inferiores a 2.5 micras\",\"metadata\":{}},\"PM25Name\":{\"type\":\"Text\",\"value\":\"PM25\",\"metadata\":{}},\"PM25Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"PM25Value\":{\"type\":\"Number\",\"value\":\"4\",\"metadata\":{}},\"PM25ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"SO2Description\":{\"type\":\"Text\",\"value\":\"Dióxido de Azufre\",\"metadata\":{}},\"SO2Name\":{\"type\":\"Text\",\"value\":\"SO2\",\"metadata\":{}},\"SO2Type\":{\"type\":\"Text\",\"value\":\"µg/m3\",\"metadata\":{}},\"SO2Value\":{\"type\":\"Number\",\"value\":\"3\",\"metadata\":{}},\"SO2ValueFlag\":{\"type\":\"Text\",\"value\":\"V\",\"metadata\":{}},\"dateObserved\":{\"type\":\"DateTime\",\"value\":\"2022-09-08T00:00:00.000Z\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.3426602,39.4575225],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"refPointOfInterest\":{\"type\":\"Text\",\"value\":\"A01_AVFRANCIA\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"OCI\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "A01_AVFRANCIA_24h",
        "type": "AirQualityObserved",
        "NO2Description": {
          "type": "Text",
          "value": "Dióxido de Nitrogeno",
          "metadata": {}
        },
        "NO2Name": {
          "type": "Text",
          "value": "NO2",
          "metadata": {}
        },
        "NO2Type": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "NO2Value": {
          "type": "Number",
          "value": "14",
          "metadata": {}
        },
        "NO2ValueFlag": {
          "type": "Text",
          "value": "V",
          "metadata": {}
        },
        "O3Description": {
          "type": "Text",
          "value": "Ozono",
          "metadata": {}
        },
        "O3Name": {
          "type": "Text",
          "value": "O3",
          "metadata": {}
        },
        "O3Type": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "O3Value": {
          "type": "Number",
          "value": "57",
          "metadata": {}
        },
        "O3ValueFlag": {
          "type": "Text",
          "value": "V",
          "metadata": {}
        },
        "PM10Aviso": {
          "type": "Number",
          "value": "80",
          "metadata": {}
        },
        "PM10CorrectionFactor": {
          "type": "Number",
          "value": "1.0",
          "metadata": {}
        },
        "PM10CorrectionFactorPrev": {
          "type": "Number",
          "value": "1.0",
          "metadata": {}
        },
        "PM10Description": {
          "type": "Text",
          "value": "Particulas en suspensión inferiores a 10 micras",
          "metadata": {}
        },
        "PM10DescriptionPrev": {
          "type": "Text",
          "value": "Particulas en suspensión inferiores a 10 micras",
          "metadata": {}
        },
        "PM10Name": {
          "type": "Text",
          "value": "PM10",
          "metadata": {}
        },
        "PM10NamePrev": {
          "type": "Text",
          "value": "PM10",
          "metadata": {}
        },
        "PM10Preaviso": {
          "type": "Number",
          "value": "50",
          "metadata": {}
        },
        "PM10Type": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "PM10TypePrev": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "PM10Value": {
          "type": "Number",
          "value": "32",
          "metadata": {}
        },
        "PM10ValueFlag": {
          "type": "Text",
          "value": "V",
          "metadata": {}
        },
        "PM10ValueFull": {
          "type": "Number",
          "value": "32.0",
          "metadata": {}
        },
        "PM10ValueFullPrev": {
          "type": "Number",
          "value": "42.0",
          "metadata": {}
        },
        "PM10ValueOrigin": {
          "type": "Number",
          "value": "31.791666666666668",
          "metadata": {}
        },
        "PM10ValueOriginPrev": {
          "type": "Number",
          "value": "42.333333333333336",
          "metadata": {}
        },
        "PM10ValuePrev": {
          "type": "Number",
          "value": "42",
          "metadata": {}
        },
        "PM25Description": {
          "type": "Text",
          "value": "Particulas en suspensión inferiores a 2.5 micras",
          "metadata": {}
        },
        "PM25Name": {
          "type": "Text",
          "value": "PM25",
          "metadata": {}
        },
        "PM25Type": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "PM25Value": {
          "type": "Number",
          "value": "4",
          "metadata": {}
        },
        "PM25ValueFlag": {
          "type": "Text",
          "value": "V",
          "metadata": {}
        },
        "SO2Description": {
          "type": "Text",
          "value": "Dióxido de Azufre",
          "metadata": {}
        },
        "SO2Name": {
          "type": "Text",
          "value": "SO2",
          "metadata": {}
        },
        "SO2Type": {
          "type": "Text",
          "value": "µg/m3",
          "metadata": {}
        },
        "SO2Value": {
          "type": "Number",
          "value": "3",
          "metadata": {}
        },
        "SO2ValueFlag": {
          "type": "Text",
          "value": "V",
          "metadata": {}
        },
        "dateObserved": {
          "type": "DateTime",
          "value": "2022-09-08T00:00:00.000Z",
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "coordinates": [
              -0.3426602,
              39.4575225
            ],
            "type": "Point"
          },
          "metadata": {}
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "OCI",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "refPointOfInterest": {
          "type": "Text",
          "value": "A01_AVFRANCIA",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "OCI",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

# Attributes sent to GIS to be used in graphic representation

===========================

- El GIS se actualiza mediante una ETL que accede al Postgres.
