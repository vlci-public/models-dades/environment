**Índice**

 [[_TOC_]]  

# Entidad: AirQualityObserved

[Basado en la entidad Fiware AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

Descripción global: **Una observación de las condiciones de calidad del aire mediante sensores embarcados en autobuses de la emt.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad.
- `type`: NGSI Tipo de entidad.

##### Atributos descriptivos de la entidad

- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `project`: Proyecto al que pertenece esta entidad.
- `idBus`: Identificador que consiste en un número entero positivo. Sólo caracteres [0-9]

##### Atributos descriptivos de la entidad (OPCIONALES)

- Ninguno.

##### Atributos de la medición

- `temperatureInt`: Temperatura en el interior del vehículo en grados celsius.
- `temperatureExt`: Temperatura en el exterior del vehículo en grados celsius.
- `relativeHumidityInt`: Porcentaje de humedad relativa en el interior del vehículo.
- `relativeHumidityExt`: Porcentaje de humedad relativa en el exterior del vehículo.
- `PM25Int`: Partículas de 2,5 micrómetros o menos de diámetro. Cantidad de partículas sólidas o líquidas de polvo, cenizas, hollín, partículas metálicas, cemento y polen, dispersas en la atmósfera, y cuyo diámetro es aproximadamente 2.5 µm. Unidad de medida: µg/m3.
- `LaeqInt`: Nivel de presión sonora constante, medido en decibelios (dB), que equivale a la energía sonora promedio durante un intervalo de tiempo específico.
- `COExt`: Concentración de monóxido de carbono en el aire exterior, medida en microgramos por metro cúbico (µg/m³).
- `NO2Ext`: Concentración de dióxido de nitrógeno en el aire exterior, medida en microgramos por metro cúbico (µg/m³).
- `O3Ext`: Concentración de ozono en el aire exterior, medida en microgramos por metro cúbico (µg/m³).
- `dateObserved`: La fecha y la hora de esta observación en formato ISO8601 UTC. La fecha/hora es el final de la medición. Por defecto las mediciones son diezminutales.
- `TimeInstant`: La fecha y hora UTC en la que la plataforma recibe el dato, expresada en formato ISO8601.

##### Atributos para realizar cálculos / lógica de negocio

- Ninguno

##### Atributos para el GIS / Representar gráficamente la entidad

- Ninguno

##### Atributos para la monitorización

- `operationalStatus`: Indica si la entidad está recibiendo datos. Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             |
|--------------------|------------------------|
| /medioambiente_emt | sensoremt6546    |

##### Ejemplo de entidad

```curl
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/sensoremt6546' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /medioambiente_emt' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

Atributos que van al GIS para su representación en una capa
===========================

- Ninguno. No existe suscripción al GIS para estas entidades
