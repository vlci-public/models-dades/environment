**Índice**  

 [[_TOC_]]  
  

# Entity: AirQualityObserved

[Based upon the Fiware entity AirQualityObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved)

General description: **An observation of the air quality conditions at a certain time in a certain place, this entity is an specification from [BSG](https://www.bsg.es/en/index) and uses the web service provided by said company. Being the entity, almost a direct translation from the data received from the web service. The data gets updated every 10 minutes.**

Uses the best practices and standards defined [here](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

**OF NOTE: Not all measuring stations provide with data for all pollutants, the following table specify which measurements are provided by each station.**

## Properties List

##### Context broker entity Attributes

- `id`: Unique identifier of the entity
- `type`: NGSI Entity Type

##### Entity description attributes

- `location`: Geojson reference, it can be a Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
- `refPointOfInterest`: Entity ID, refers to a PointOfInterest entity type, wich describes the station

##### (OPTIONAL) Entity description attributes

- None

##### Measurement attributes

- `dateObserved`: The date and time of the measurement in ISO8601 format, converted to the Europe/Madrid timezone
- `dateObservedGMT0`: The date and time of this observation in ISO8601 UTCformat. Model: [https://schema.org/Text](https://schema.org/Text)
- `NO2ValueOrigin`: Nitrogen dioxide measured, exactly as received from the origin system (With no format applied)
- `NO2Value`: Nitrogen dioxide measured, formatted into an integer
- `NO2ValueFlag`: Posible values: V or F. V means valid measurement, any other value means not valid measurement
- `O3ValueOrigin`: Ozone measured, exactly as received from the origin system (With no format applied)
- `O3Value`: Ozone measured, formatted into an integer
- `O3ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM10Value`: Particulate matter 10 micrometers or fewer in diameter, formatted into an integer
- `PM10ValueFull`: Particulate matter 10 micrometers or fewer in diameter with a correction factor applied on ValueOrigin, not rounded
- `PM10ValueOrigin`: Particulate matter 10 micrometers or fewer in diameter, exactly as in the origin system (With no format applied)
- `PM25Value`: Particulate matter 2,5 micrometers or fewer in diameter, formatted into an integer
- `PM25ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM25ValueOrigin`: Particulate matter 2,5 micrometers or fewer in diameter, exactly as received from the origin system (With no format applied)
- `SO2Value`: Sulphur dioxide measured, formatted into an integer
- `SO2ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `SO2ValueOrigin`: Sulphur dioxide measured, exactly as received from the origin system (With no format applied)
- `NOValue`: Nitrogen monoxide measured, formatted into an integer
- `NOValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `NOValueOrigin`: Nitrogen monoxide measured, exactly as received from the origin system (With no format applied)
- `NOXValue`: Nitric monoxide measured, formatted into an integer
- `NOXValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `NOXValueOrigin`: Nitrogen monoxide measured, exactly as received from the origin system (With no format applied)
- `PM1Value`: Particulate matter 1 micrometer or less in diameter, formatted into an integer
- `PM1ValueFlag`: Possible values: V or F. V means valid measurement, any other value means not valid measurement
- `PM1ValueOrigin`: Particulate matter 1 micrometer or less in diameter, exactly as in the origin system (With no format applied)

##### Measurement Description attributes

- `NO2Description`: Measurement description
- `NO2Name`: Pollutant name (Ex: NO2)
- `NO2Type`: Measurement unit (µg/m3)
- `O3Description`: Measurement description
- `O3Name`: Pollutant name (Ex: NO2)
- `O3Type`: Measurement unit (µg/m3)
- `PM10CorrectionFactor`: Correction Factor to be applied to the PM10 measurement
- `PM10Description`: Measurement description
- `PM10Name`: Pollutant name (Ex: NO2)
- `PM10Type`: Measurement unit (µg/m3)
- `PM25Description`: Measurement description
- `PM25Name`: Pollutant name (Ex: NO2)
- `PM25Type`: Measurement unit (µg/m3)
- `SO2Description`: Measurement description
- `SO2Name`: Pollutant name (Ex: NO2)
- `SO2Type`: Measurement unit (µg/m3)
- `NODescription`: Measurement description
- `NOName`: Pollutant name (Ex: NO2)
- `NOType`: Measurement unit (µg/m3)
- `NOXDescription`: Measurement description
- `NOXName`: Pollutant name (Ex: NO2)
- `NOXType`: Measurement unit (µg/m3)
- `PM1Description`: Measurement description
- `PM1Name`: Pollutant name (Ex: NO2)
- `PM1Type`: Measurement unit (µg/m3)

##### Attributes to be used in calculations / Business logic

- None

##### GIS attributes / Entity graphical representation

- None

##### Monitoring Attributes

- `operationalStatus`: Possible values: ok, noData
- `maintenanceOwner`: Technical manager for this entity
- `maintenanceOwnerEmail`: Mailing address from the technical manager
- `serviceOwner`: Municipal worker in charge of the entity
- `serviceOwnerEmail`: Mailing address from the municipal worker

##### Unnecessary attributes in future implementations

- None

## Entity list and Pollutants measured

| Sub service    | Entity ID              | Pollutants measured      |
| -------------- | ---------------------- | ------------------------ |
| /MedioAmbiente | A01_AVFRANCIA_10m      | NO2,O3,PM10,PM25,SO2     |
| /MedioAmbiente | A02_BULEVARDSUD_10m    | NO2,O3,SO2               |
| /MedioAmbiente | A03_MOLISOL_10m        | NO2,O3,PM10,PM25,SO2     |
| /MedioAmbiente | A04_PISTASILLA_10m     | NO2,O3,PM10,PM25,SO2     |
| /MedioAmbiente | A05_POLITECNIC_10m     | NO2,O3,PM10,PM25,SO2     |
| /MedioAmbiente | A06_VIVERS_10m         | NO2,O3,SO2               |
| /MedioAmbiente | A07_VALENCIACENTRE_10m | NO2,PM10,PM25            |
| /MedioAmbiente | A08_DR_LLUCH_10m       | NO2,PM10,PM25,NO,NOX,PM1 |
| /MedioAmbiente | A09_CABANYAL_10m       | NO2,PM10,PM25,NO,NOX,PM1 |
| /MedioAmbiente | A10_OLIVERETA_10m      | NO2,PM10,PM25,NO,NOX     |
| /MedioAmbiente | A11_PATRAIX_10m        | NO2,PM10,PM25,NO,NOX     |

##### Entity example

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/A01_AVFRANCIA_10m' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /MedioAmbiente' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Context Broker (NGSI v2) subscription data sample

<details><summary>Click para ver la respuesta</summary>

HEADERS:

Host: XXXXX
X-Amzn-Trace-Id: Root=XXXXX
Content-Length: 2819
user-agent: orion/3.7.0 libcurl/7.74.0
fiware-service: sc_vlci
fiware-servicepath: /MedioAmbiente
x-auth-token: XXXXX
accept: application/json
content-type: application/json; charset=utf-8
fiware-correlator: 108aa4b3-a40e-488f-b330-282bdf15442e; cbnotif=7
ngsiv2-attrsformat: normalized

BODY:

{
"subscriptionId": "6388812f1e5e7d0ef326a432",
"data": [
{
"id": "A01_AVFRANCIA_10m",
"type": "AirQualityObserved",
"NO2Description": {
"type": "Text",
"value": "DiÃ³xido de Nitrogeno",
"metadata": {}
},
"NO2Name": {
"type": "Text",
"value": "NO2",
"metadata": {}
},
"NO2Type": {
"type": "Text",
"value": "Âµg/m3",
"metadata": {}
},
"NO2Value": {
"type": "Number",
"value": "6",
"metadata": {}
},
"NO2ValueFlag": {
"type": "Text",
"value": "V",
"metadata": {}
},
"NO2ValueOrigin": {
"type": "Number",
"value": 6,
"metadata": {}
},
"O3Description": {
"type": "Text",
"value": "Ozono",
"metadata": {}
},
"O3Name": {
"type": "Text",
"value": "O3",
"metadata": {}
},
"O3Type": {
"type": "Text",
"value": "Âµg/m3",
"metadata": {}
},
"O3Value": {
"type": "Number",
"value": "56",
"metadata": {}
},
"O3ValueFlag": {
"type": "Text",
"value": "V",
"metadata": {}
},
"O3ValueOrigin": {
"type": "Number",
"value": 56,
"metadata": {}
},
"PM10CorrectionFactor": {
"type": "Number",
"value": "1.0",
"metadata": {}
},
"PM10Description": {
"type": "Text",
"value": "Particulas en suspensiÃ³n inferiores a 10 micras",
"metadata": {}
},
"PM10Name": {
"type": "Text",
"value": "PM10",
"metadata": {}
},
"PM10Type": {
"type": "Text",
"value": "Âµg/m3",
"metadata": {}
},
"PM10Value": {
"type": "Number",
"value": "39",
"metadata": {}
},
"PM10ValueFlag": {
"type": "Text",
"value": "V",
"metadata": {}
},
"PM10ValueFull": {
"type": "Number",
"value": "38.5",
"metadata": {}
},
"PM10ValueOrigin": {
"type": "Number",
"value": 38.5,
"metadata": {}
},
"PM25Description": {
"type": "Text",
"value": "Particulas en suspensiÃ³n inferiores a 2.5 micras",
"metadata": {}
},
"PM25Name": {
"type": "Text",
"value": "PM25",
"metadata": {}
},
"PM25Type": {
"type": "Text",
"value": "Âµg/m3",
"metadata": {}
},
"PM25Value": {
"type": "Number",
"value": "6",
"metadata": {}
},
"PM25ValueFlag": {
"type": "Text",
"value": "V",
"metadata": {}
},
"PM25ValueOrigin": {
"type": "Number",
"value": 6.1,
"metadata": {}
},
"SO2Description": {
"type": "Text",
"value": "DiÃ³xido de Azufre",
"metadata": {}
},
"SO2Name": {
"type": "Text",
"value": "SO2",
"metadata": {}
},
"SO2Type": {
"type": "Text",
"value": "Âµg/m3",
"metadata": {}
},
"SO2Value": {
"type": "Number",
"value": "3",
"metadata": {}
},
"SO2ValueFlag": {
"type": "Text",
"value": "V",
"metadata": {}
},
"SO2ValueOrigin": {
"type": "Number",
"value": 2.9,
"metadata": {}
},
"dateObserved": {
"type": "DateTime",
"value": "2022-12-01T11:20:00.000Z",
"metadata": {}
},
"dateObservedGMT0": {
"type": "DateTime",
"value": "2022-12-01T10:20:00.000Z",
"metadata": {}
},
"location": {
"type": "geo:json",
"value": {
"coordinates": [
-0.3426602,
39.4575225
],
"type": "Point"
},
"metadata": {}
},
"maintenanceOwner": {
"type": "Text",
"value": "OCI",
"metadata": {}
},
"maintenanceOwnerEmail": {
"type": "Text",
"value": "integracionvlci@valencia.es",
"metadata": {}
},
"operationalStatus": {
"type": "Text",
"value": "ok",
"metadata": {}
},
"refPointOfInterest": {
"type": "Text",
"value": "A01_AVFRANCIA",
"metadata": {}
},
"serviceOwner": {
"type": "Text",
"value": "OCI",
"metadata": {}
},
"serviceOwnerEmail": {
"type": "Text",
"value": "integracionvlci@valencia.es",
"metadata": {}
}
}
]
}

</details>

# Attributes sent to GIS to be used in graphic representation

- None. No GIS subscription exist for this entities
