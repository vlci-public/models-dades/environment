**Índice**  

 [[_TOC_]]  
  

# Entidad: NoiseLevelObserved

[Basado en la entidad Fiware NoiseLevelObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/NoiseLevelObserved)

Descripción global: **Una observación de aquellos parámetros acústicos que estiman los niveles de presión acústica en un lugar y momento determinados.**

Estas entidades modelan los sonómetros instalados en zonas ZAS de Valencia.

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad

- `name[string]`: El nombre de este artículo.
- `location[geo:json]`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `address[string]`: La dirección postal.
- `project[string]`: proyecto al que pertenece esta entidad.

##### Atributos de la medición

- `dateObserved`: Fecha y hora del periodo de observación.
- `LAeq`: El índice LAeq, o nivel sonoro equivalente A, mide el nivel promedio de sonido a lo largo de un período de tiempo, ajustado según la ponderación A. Básicamente, representa la energía sonora total acumulada y nos dice cómo se percibe un ruido fluctuante o intermitente como si fuera un ruido constante.
- `lAeqReal`: Valor medio en decibelios (dB) registrado durante un minuto concreto. Este valor no estará redondeado, es el original. 
- `LAFmax`: Valor máximo en decibelios (dB) registrado durante un minuto concreto. Este valor estará redondeado a un único decimal
- `LAFmaxReal `: Valor máximo en decibelios (dB) registrados durante un minuto concreto. Este valor no estará redondeado, es el original.
- `LAFmin  `: Valor mínimo en decibelios (dB) registrado durante un minuto concreto. Este valor estará redondeado a un decimal.
- `LAFminReal `: Valor mínimo en decibelios (dB) registrados durante un minuto concreto. Este valor no estará redondeado, es el original.
- `lAIeq`: Valor medio en decibelios (dB) registrado durante un minuto concreto con la constante temporal impulsiva. Este valor estará redondeado a un decimal
- `LAIeqReal `: Valor medio en decibelios (dB) registrado durante un minuto concreto con la constante temporal impulsiva. Este valor estará no estará redondeado, es el original.
- `timeInstant`: La fecha y la hora del envío del dato del agente IoT.

##### Atributos para la monitorización

- `operationalStatus`: Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Entidades que implementan este modelo (18 entidades)

| Subservicio | ID Entidad                   |
| ----------- | ---------------------------- |
| /sonometros | sonzas.*  |
