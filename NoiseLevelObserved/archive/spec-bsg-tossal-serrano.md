**Índice**  

 [[_TOC_]]  
  

# Entidad: NoiseLevelObserved

[Basado en la entidad Fiware NoiseLevelObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/NoiseLevelObserved)

Descripción global: **Una observación de aquellos parámetros acústicos que estiman los niveles de presión acústica en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `address`: La dirección postal  
- `project`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)
- `measurand`: 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.  
- `refDevice`: Una referencia al dispositivo que captó esta observación.  

##### Atributos de la medición
- `dateObservedFrom`: Fecha y hora de inicio del periodo de observación.  
- `dateObservedTo`: Fecha y hora de finalización del periodo de observación. Véase dateObserved.  
- `LAeq`: El índice LAeq, o nivel sonoro equivalente A, mide el nivel promedio de sonido a lo largo de un período de tiempo, ajustado según la ponderación A. Básicamente, representa la energía sonora total acumulada y nos dice cómo se percibe un ruido fluctuante o intermitente como si fuera un ruido constante.

##### Atributos para realizar cálculos / lógica de negocio
- `eveningLAEqLimit`: dB en el tramo de la tarde a partir de los cuales se marca en rojo un sensor de ruido
- `eveningTimeLimit`: Define la hora fin del tramo de la tarde
- `morningLAEqLimit`: dB en el tramo de la mañana a partir de los cuales se marca en rojo un sensor de ruido
- `morningTimeLimit`: Define la hora fin del tramo de la mañana
- `nightLAEqLimit`: dB en el tramo de la noche a partir de los cuales se marca en rojo un sensor de ruido
- `nightTimeLimit`: Define la hora fin del tramo de la noche
- `calculatedTimestamp`: Define el momento en el que la entidad a sido actualizada con todos los datos calculados

##### Atributos para el GIS / Representar gráficamente la entidad
- `noiseLevelLAEq`: Para el GIS. Posibles valores: verde, rojo, gris. Verde implica estar por debajo del límite para el tramo horario. Rojo implica estar por encima del límite en el tramo horario. Gris significa sin datos recientes.

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `owner`: Una lista que contiene una secuencia de caracteres codificada en JSON que hace referencia a los identificadores únicos de los propietarios  
- `ownerEmail`: Email que identifica el responsable de esta entidad
- `respOCI`: Funcionario responsable de esta entidad en la OCI
- `respOCIEmail`: Email del funcionario  responsable de esta entidad en la OCI
- `respOCITecnico`: Responsable técnico de esta entidad en la OCI
- `respOCITecnicoEmail`: Email del responsable técnico de esta entidad en la OCI
- `respServicio`: Persona del servicio municipal de contacto para esta entidad
- `respServicioEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos extra, específicos de BSG
- `tokenLuminaria`: Campo para actualizar las luminarias de la plaza Tossal

##### Atributos innecesarios en futuras entidades
- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad  |
|----------------|-------------|
| /sonometros    | 2693        |
| /sonometros    | 2699        |

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2236",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/sonometros",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "f686ddd8-e7b3-4d7b-9594-f59842e708cd; cbnotif=1; node=2I_aNjd2H1; perseocep=1477030; cbnotif=3",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"2693\",\"type\":\"NoiseLevelObserved\",\"LAeq\":{\"type\":\"Float\",\"value\":\"46.2\",\"metadata\":{}},\"address\":{\"type\":\"String\",\"value\":\"Plaça del Tossal\",\"metadata\":{}},\"calculatedTimestamp\":{\"type\":\"Timestamp\",\"value\":\"2022-09-19T07:53:00+00:00\",\"metadata\":{}},\"dateObservedFrom\":{\"type\":\"Timestamp\",\"value\":\"2022-09-19T07:52:00+00:00\",\"metadata\":{}},\"dateObservedTo\":{\"type\":\"Timestamp\",\"value\":\"2022-09-19T07:53:00+00:00\",\"metadata\":{}},\"eveningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"eveningTimeLimit\":{\"type\":\"Number\",\"value\":\"23\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-0.380236,39.476128]},\"metadata\":{}},\"measurand\":{\"type\":\"compound\",\"value\":[\"LAeq  | 67.8 | A-weighted equivalent sound level\"],\"metadata\":{}},\"morningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"morningTimeLimit\":{\"type\":\"Number\",\"value\":\"21\",\"metadata\":{}},\"name\":{\"type\":\"String\",\"value\":\"Sonometro Plaça del Tossal\",\"metadata\":{}},\"nightLAEqLimit\":{\"type\":\"Number\",\"value\":55,\"metadata\":{}},\"nightTimeLimit\":{\"type\":\"Number\",\"value\":\"7\",\"metadata\":{}},\"noiseLevelLAEq\":{\"type\":\"Text\",\"value\":\"verde\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"inactive\":{\"type\":\"boolean\",\"value\":\"false\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"project\":{\"type\":\"Text\",\"value\":\"BSG - bsg\",\"metadata\":{}},\"refDevice\":{\"type\":\"Relationship\",\"value\":\"device-2693\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"Equipo Integración\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"BSG\",\"metadata\":{}},\"tokenLuminaria\":{\"type\":\"String\",\"value\":\"ac680bfa250052cab39fd6c438e86e62db4b9d04\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "2693",
        "type": "NoiseLevelObserved",
        "LAeq": {
          "type": "Float",
          "value": "46.2",
          "metadata": {}
        },
        "address": {
          "type": "String",
          "value": "Plaça del Tossal",
          "metadata": {}
        },
        "calculatedTimestamp": {
          "type": "Timestamp",
          "value": "2022-09-19T07:53:00+00:00",
          "metadata": {}
        },
        "dateObservedFrom": {
          "type": "Timestamp",
          "value": "2022-09-19T07:52:00+00:00",
          "metadata": {}
        },
        "dateObservedTo": {
          "type": "Timestamp",
          "value": "2022-09-19T07:53:00+00:00",
          "metadata": {}
        },
        "eveningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "eveningTimeLimit": {
          "type": "Number",
          "value": "23",
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [-0.380236, 39.476128]
          },
          "metadata": {}
        },
        "measurand": {
          "type": "compound",
          "value": ["LAeq  | 67.8 | A-weighted equivalent sound level"],
          "metadata": {}
        },
        "morningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "morningTimeLimit": {
          "type": "Number",
          "value": "21",
          "metadata": {}
        },
        "name": {
          "type": "String",
          "value": "Sonometro Plaça del Tossal",
          "metadata": {}
        },
        "nightLAEqLimit": {
          "type": "Number",
          "value": 55,
          "metadata": {}
        },
        "nightTimeLimit": {
          "type": "Number",
          "value": "7",
          "metadata": {}
        },
        "noiseLevelLAEq": {
          "type": "Text",
          "value": "verde",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "inactive": {
          "type": "boolean",
          "value": "false",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "project": {
          "type": "Text",
          "value": "BSG - bsg",
          "metadata": {}
        },
        "refDevice": {
          "type": "Relationship",
          "value": "device-2693",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "Equipo Integración",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "BSG",
          "metadata": {}
        },
        "tokenLuminaria": {
          "type": "String",
          "value": "XXXXX",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================

##### Atributos de la entidad context broker
- id
- type

##### Atributos descriptivos de la entidad
- name
- location
- address
- project

##### Atributos de la medición
- dateObservedFrom
- dateObservedTo
- LAeq

##### Atributos para el GIS / Representar gráficamente la entidad
- noiseLevelLAEq

##### Atributos para la monitorización
- operationalStatus
- owner, 
- ownerEmail, 
- respServicio, 
- respServicioEmail
