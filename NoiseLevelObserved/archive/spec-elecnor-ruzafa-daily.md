**Índice**  

 [[_TOC_]]  
  

# Entidad: NoiseLevelObserved

[Basado en la entidad Fiware NoiseLevelObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/NoiseLevelObserved)

Descripción global: **Una observación de aquellos parámetros acústicos que estiman los niveles de presión acústica en un lugar y momento determinados.**  

Estas entidades contienen medidas diarias calculadas por la plataforma de CESVA, a partir de los datos minutales.

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `address`: La dirección postal.
- `project`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)
- `serialNumber`: 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.  
- `refDevice`: Una referencia al dispositivo que captó esta observación.  
- `measurand`: 

##### Atributos de la medición
- `dateObserved`: Fecha y hora de finalización del periodo de observación. 
- `dateObservedFrom`: Fecha y hora de inicio del periodo de observación.
- `dateObservedTo`: Fecha y hora de finalización del periodo de observación.
- `TimeInstant`:  La fecha y la hora del envío del dato del agente IoT.
- `LAeq`: Media de ruido del día completo.
- `LAeq_d`: Media de ruido de la mañana de un día.
- `LAeq_den`:  <!-- TO-DO Explicación del campo -->
- `LAeq_e`: Media de ruido de la tarde de un día.
- `LAeq_n`: Media de ruido de la noche de un día.
- `LA_max`:  <!-- TO-DO Explicación del campo -->

##### Atributos para realizar cálculos / lógica de negocio
- `calculatedTimestamp`: Define el momento en el que la entidad a sido actualizada con todos los datos calculados

##### Atributos para el GIS / Representar gráficamente la entidad
- `noiseLevelLAEq`: Para el GIS. Posibles valores: verde, rojo, gris. Verde implica estar por debajo del límite para el tramo horario. Rojo implica estar por encima del límite en el tramo horario. Gris significa sin datos recientes. 

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `owner`: Una lista que contiene una secuencia de caracteres codificada en JSON que hace referencia a los identificadores únicos de los propietarios  
- `ownerEmail`: Email que identifica el responsable de esta entidad
- `respOCI`: Funcionario responsable de esta entidad en la OCI
- `respOCIEmail`: Email del funcionario  responsable de esta entidad en la OCI
- `respOCITecnico`: Responsable técnico de esta entidad en la OCI
- `respOCITecnicoEmail`: Email del responsable técnico de esta entidad en la OCI
- `respServicio`: Persona del servicio municipal de contacto para esta entidad
- `respServicioEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos deprecados que **ya no existen** en las entidades actuales pero si en los historicos
- `ignoreAttribute`: Atributo que viene del IoT Agent, que no necesitamos.

##### Atributos innecesarios en futuras entidades
- Ninguno

## Lista de entidades que implementan este modelo (16 entidades)

| Subservicio    | ID Entidad    |
|----------------|---------------|
| /sonometros    | T248652-daily |
| /sonometros    | T248655-daily |
| /sonometros    | T248661-daily |
| /sonometros    | T248669-daily |
| /sonometros    | T248670-daily |
| /sonometros    | T248671-daily |
| /sonometros    | T248672-daily |
| /sonometros    | T248676-daily |
| /sonometros    | T248677-daily |
| /sonometros    | T248678-daily |
| /sonometros    | T248679-daily |
| /sonometros    | T248680-daily |
| /sonometros    | T248682-daily |
| /sonometros    | T248683-daily |
| /sonometros    | T248684-daily |
| /sonometros    | T251234-daily |

##### Ejemplo de entidad
```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/T248683-daily' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /sonometros' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)
<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2192",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci_pre",
    "fiware-servicepath": "/sonometros",
    "x-auth-token": "XXXXX",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "75e66d9a-a820-4108-be8b-f5bc01caedb3; cbnotif=1",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"T248683-daily\",\"type\":\"NoiseLevelObserved\",\"LAeq\":{\"type\":\"Number\",\"value\":55.15,\"metadata\":{}},\"LAeq_d\":{\"type\":\"Number\",\"value\":58.03,\"metadata\":{}},\"LAeq_den\":{\"type\":\"Number\",\"value\":55.53,\"metadata\":{}},\"LAeq_e\":{\"type\":\"Number\",\"value\":46.76,\"metadata\":{}},\"LAeq_n\":{\"type\":\"Number\",\"value\":36.52,\"metadata\":{}},\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-10-25T05:00:00.000Z\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"Sueca 2\",\"metadata\":{}},\"calculatedTimestamp\":{\"type\":\"Timestamp\",\"value\":\"2021-11-03\",\"metadata\":{}},\"dateObserved\":{\"type\":\"Timestamp\",\"value\":\"2022-10-25\",\"metadata\":{}},\"dateObservedFrom\":{\"type\":\"DateTime\",\"value\":\"2022-10-25T05:00:00.000Z\",\"metadata\":{}},\"dateObservedTo\":{\"type\":\"DateTime\",\"value\":\"2022-10-26T04:59:59.999Z\",\"metadata\":{}},\"description\":{\"type\":\"Text\",\"value\":\"Entidad de actualización diaria de 06:00 a 06:00\",\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.3766378,39.4633107],\"type\":\"Point\"},\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"Sensor de ruido - Sueca 2\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"noData\",\"metadata\":{}},\"inactive\":{\"type\":\"boolean\",\"value\":false,\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"project\":{\"type\":\"Text\",\"value\":\"Elecnor - ruidoval\",\"metadata\":{}},\"refDevice\":{\"type\":\"Text\",\"value\":\"T248683\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serialNumber\":{\"type\":\"Integer\",\"value\":\"T248683\",\"metadata\":{}},\"sonometerClass\":{\"type\":\"Number\",\"value\":1,\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"CESVA\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "T248683-daily",
        "type": "NoiseLevelObserved",
        "LAeq": {
          "type": "Number",
          "value": 55.15,
          "metadata": {}
        },
        "LAeq_d": {
          "type": "Number",
          "value": 58.03,
          "metadata": {}
        },
        "LAeq_den": {
          "type": "Number",
          "value": 55.53,
          "metadata": {}
        },
        "LAeq_e": {
          "type": "Number",
          "value": 46.76,
          "metadata": {}
        },
        "LAeq_n": {
          "type": "Number",
          "value": 36.52,
          "metadata": {}
        },
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-10-25T05:00:00.000Z",
          "metadata": {}
        },
        "address": {
          "type": "Text",
          "value": "Sueca 2",
          "metadata": {}
        },
        "calculatedTimestamp": {
          "type": "Timestamp",
          "value": "2021-11-03",
          "metadata": {}
        },
        "dateObserved": {
          "type": "Timestamp",
          "value": "2022-10-25",
          "metadata": {}
        },
        "dateObservedFrom": {
          "type": "DateTime",
          "value": "2022-10-25T05:00:00.000Z",
          "metadata": {}
        },
        "dateObservedTo": {
          "type": "DateTime",
          "value": "2022-10-26T04:59:59.999Z",
          "metadata": {}
        },
        "description": {
          "type": "Text",
          "value": "Entidad de actualización diaria de 06:00 a 06:00",
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "coordinates": [-0.3766378, 39.4633107],
            "type": "Point"
          },
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "Sensor de ruido - Sueca 2",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "noData",
          "metadata": {}
        },
        "inactive": {
          "type": "boolean",
          "value": false,
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "project": {
          "type": "Text",
          "value": "Elecnor - ruidoval",
          "metadata": {}
        },
        "refDevice": {
          "type": "Text",
          "value": "T248683",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serialNumber": {
          "type": "Integer",
          "value": "T248683",
          "metadata": {}
        },
        "sonometerClass": {
          "type": "Number",
          "value": 1,
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "CESVA",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- Ninguno
