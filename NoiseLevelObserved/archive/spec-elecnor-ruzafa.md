**Índice**  

 [[_TOC_]]  
  

# Entidad: NoiseLevelObserved

[Basado en la entidad Fiware NoiseLevelObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/NoiseLevelObserved)

Descripción global: **Una observación de aquellos parámetros acústicos que estiman los niveles de presión acústica en un lugar y momento determinados.**

Las entidades de Elecnor son aquellas cuyo ID empieza por T y a continuación son números.
Además el atributo project = Elecnor - ruidoval 

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.  
- `address`: La dirección postal. 
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `project`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)
- `serialNumber`: 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.  
- `refDevice`: Una referencia al dispositivo que captó esta observación.  
- `sonometerClass`: Clase de sonómetro (0, 1, 2) según ANSI utilizado para tomar esta observación
- `measurand`:

##### Atributos de la medición
- `dateObserved`: Fecha y hora de finalización del periodo de observación. 
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `LAeq`: El índice LAeq, o nivel sonoro equivalente A, mide el nivel promedio de sonido a lo largo de un período de tiempo, ajustado según la ponderación A. Básicamente, representa la energía sonora total acumulada y nos dice cómo se percibe un ruido fluctuante o intermitente como si fuera un ruido constante.

##### Atributos para realizar cálculos / lógica de negocio
- `eveningLAEqLimit`: dB en el tramo de la tarde a partir de los cuales se marca en rojo un sensor de ruido
- `eveningTimeLimit`: Define la hora fin del tramo de la tarde
- `morningLAEqLimit`: dB en el tramo de la mañana a partir de los cuales se marca en rojo un sensor de ruido
- `morningTimeLimit`: Define la hora fin del tramo de la mañana
- `nightLAEqLimit`: dB en el tramo de la noche a partir de los cuales se marca en rojo un sensor de ruido
- `nightTimeLimit`: Define la hora fin del tramo de la noche
- `calculatedTimestamp`: Define el momento en el que la entidad a sido actualizada con todos los datos calculados

##### Atributos para el GIS / Representar gráficamente la entidad
- `noiseLevelLAEq`: Para el GIS. Posibles valores: verde, rojo, gris. Verde implica estar por debajo del límite para el tramo horario. Rojo implica estar por encima del límite en el tramo horario. Gris significa sin datos recientes.

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `owner`: Una lista que contiene una secuencia de caracteres codificada en JSON que hace referencia a los identificadores únicos de los propietarios  
- `ownerEmail`: Email que identifica el responsable de esta entidad
- `respOCI`: Funcionario responsable de esta entidad en la OCI
- `respOCIEmail`: Email del funcionario  responsable de esta entidad en la OCI
- `respOCITecnico`: Responsable técnico de esta entidad en la OCI
- `respOCITecnicoEmail`: Email del responsable técnico de esta entidad en la OCI
- `respServicio`: Persona del servicio municipal de contacto para esta entidad
- `respServicioEmail`: Email de la persona del servicio municipal de contacto para esta entidad
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad

##### Atributos deprecados que **ya no existen** en las entidades actuales pero si en los historicos
- `ef`: Atributo que viene del IoT Agent, que no necesitamos.
- `en`: Atributo que viene del IoT Agent, que no necesitamos.
- `eo`: Atributo que viene del IoT Agent, que no necesitamos.
- `eu`: Atributo que viene del IoT Agent, que no necesitamos.
- `ignoreAttribute`: Atributo que viene del IoT Agent, que no necesitamos.

## Lista de entidades que implementan este modelo (16 entidades)

| Subservicio    | ID Entidad  |
|----------------|-------------|
| /sonometros    | T248652     |
| /sonometros    | T248655     |
| /sonometros    | T248661     |
| /sonometros    | T248669     |
| /sonometros    | T248670     |
| /sonometros    | T248671     |
| /sonometros    | T248672     |
| /sonometros    | T248676     |
| /sonometros    | T248677     |
| /sonometros    | T248678     |
| /sonometros    | T248679     |
| /sonometros    | T248680     |
| /sonometros    | T248682     |
| /sonometros    | T248683     |
| /sonometros    | T248684     |
| /sonometros    | T251234     |

##### Ejemplo de entidad
```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/T248683' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /sonometros' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)
<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2793",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/sonometros",
    "x-auth-token": "XXXXX",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "027c4eca-5084-4723-996a-e43ddde16c54; cbnotif=1",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"T248683\",\"type\":\"NoiseLevelObserved\",\"LAeq\":{\"type\":\"Number\",\"value\":59.1,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-10-26T08:55:03.000Z\"}}},\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-10-26T08:55:03.000Z\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"Sueca 2\",\"metadata\":{}},\"calculatedTimestamp\":{\"type\":\"timestamp\",\"value\":\"2022-05-12T09:28:00Z\",\"metadata\":{}},\"dateObserved\":{\"type\":\"Timestamp\",\"value\":\"2021-12-30T23:58:58Z\",\"metadata\":{}},\"dateObservedFrom\":{\"type\":\"DateTime\",\"value\":\"2020-12-28T18:00:00.000Z\",\"metadata\":{}},\"dateObservedTo\":{\"type\":\"DateTime\",\"value\":\"2020-12-28T18:00:00.000Z\",\"metadata\":{}},\"eveningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"eveningTimeLimit\":{\"type\":\"Number\",\"value\":23,\"metadata\":{}},\"ignoreAttribute\":{\"type\":\"double\",\"value\":100,\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"coordinates\":[-0.3766378,39.4633107],\"type\":\"Point\"},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"morningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"morningTimeLimit\":{\"type\":\"Number\",\"value\":21,\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"Sensor de ruido - Sueca 2\",\"metadata\":{}},\"nightLAEqLimit\":{\"type\":\"Number\",\"value\":55,\"metadata\":{}},\"nightTimeLimit\":{\"type\":\"Number\",\"value\":7,\"metadata\":{}},\"noiseLevelLAEq\":{\"type\":\"Text\",\"value\":\"gris\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-10-26T08:55:03.000Z\"}}},\"inactive\":{\"type\":\"boolean\",\"value\":false,\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"Ayto. Valencia\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"project\":{\"type\":\"Text\",\"value\":\"ruzafa\",\"metadata\":{}},\"refDevice\":{\"type\":\"Text\",\"value\":\"T248683\",\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serialNumber\":{\"type\":\"Integer\",\"value\":\"T248683\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"sonometerClass\":{\"type\":\"Number\",\"value\":1,\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"CESVA\",\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "T248683",
        "type": "NoiseLevelObserved",
        "LAeq": {
          "type": "Number",
          "value": 59.1,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-10-26T08:55:03.000Z"
            }
          }
        },
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-10-26T08:55:03.000Z",
          "metadata": {}
        },
        "address": {
          "type": "Text",
          "value": "Sueca 2",
          "metadata": {}
        },
        "calculatedTimestamp": {
          "type": "timestamp",
          "value": "2022-05-12T09:28:00Z",
          "metadata": {}
        },
        "dateObserved": {
          "type": "Timestamp",
          "value": "2021-12-30T23:58:58Z",
          "metadata": {}
        },
        "dateObservedFrom": {
          "type": "DateTime",
          "value": "2020-12-28T18:00:00.000Z",
          "metadata": {}
        },
        "dateObservedTo": {
          "type": "DateTime",
          "value": "2020-12-28T18:00:00.000Z",
          "metadata": {}
        },
        "eveningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "eveningTimeLimit": {
          "type": "Number",
          "value": 23,
          "metadata": {}
        },
        "ignoreAttribute": {
          "type": "double",
          "value": 100,
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "coordinates": [-0.3766378, 39.4633107],
            "type": "Point"
          },
          "metadata": {}
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "morningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "morningTimeLimit": {
          "type": "Number",
          "value": 21,
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "Sensor de ruido - Sueca 2",
          "metadata": {}
        },
        "nightLAEqLimit": {
          "type": "Number",
          "value": 55,
          "metadata": {}
        },
        "nightTimeLimit": {
          "type": "Number",
          "value": 7,
          "metadata": {}
        },
        "noiseLevelLAEq": {
          "type": "Text",
          "value": "gris",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-10-26T08:55:03.000Z"
            }
          }
        },
        "inactive": {
          "type": "boolean",
          "value": false,
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "Ayto. Valencia",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "project": {
          "type": "Text",
          "value": "ruzafa",
          "metadata": {}
        },
        "refDevice": {
          "type": "Text",
          "value": "T248683",
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serialNumber": {
          "type": "Integer",
          "value": "T248683",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "sonometerClass": {
          "type": "Number",
          "value": 1,
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "CESVA",
          "metadata": {}
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================

##### Atributos de la entidad context broker
- id
- type


##### Atributos descriptivos de la entidad
- name
- location
- project

##### Atributos de la medición
- dateObserved
- LAeq

##### Atributos para el GIS / Representar gráficamente la entidad
- noiseLevelLAEq

##### Atributos para la monitorización
- operationalStatus
- owner
- ownerEmail
- respServicio
- respServicioEmail
