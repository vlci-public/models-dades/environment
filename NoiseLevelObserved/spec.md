**Índice**  

 [[_TOC_]]  
  

# Entidad: NoiseLevelObserved

[Basado en la entidad Fiware NoiseLevelObserved](https://github.com/smart-data-models/dataModel.Environment/tree/master/NoiseLevelObserved)

Descripción global: **Una observación de aquellos parámetros acústicos que estiman los niveles de presión acústica en un lugar y momento determinados.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id[string]`: Identificador único de la entidad
- `type[string]`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `name[string]`: El nombre de este artículo.
- `location[geo:json]`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `address[string]`: La dirección postal
- `project[string]`: proyecto al que pertenece esta entidad

##### Atributos descriptivos de la entidad (OPCIONALES)

- `frequencies`: Specifies the frequencies collected from the sensor represented by the ISO 3741:2010 standard frequencies of 100 Hz to 10 000 Hz one-third octave band. The value of each frequency is the A-weighted decibel value recorded.
- `sonometerClass`: Clase de sonómetro (0, 1, 2) según ANSI utilizado para tomar esta observación
- `refDevice`: Una referencia al dispositivo que captó esta observación.
- `owner`: Una lista que contiene una secuencia de caracteres codificada en JSON que hace referencia a los identificadores únicos de los propietarios.

##### Atributos descriptivos de las entidaded daily

- `areaServed`: Agrupa varios dispositivos por responsable, distrito, barrio, etc.

##### Atributos de la medición

- `dateObservedFrom`: Fecha y hora de inicio del periodo de observación.
- `dateObservedTo`: Fecha y hora de finalización del periodo de observación. Véase dateObserved.
- `LAeq`: Nivel de sonido continuo equivalente. (dBa).
- `LA`. Nivel de sonido en dBa (LAeqT 1min). (dBa)
- `LA1`: Indican que el nivel de presión sonora LA ha sido sobrepasado en un porcentaje 1% del tiempo o período de medición T.
- `LA10`: Indican que el nivel de presión sonora LA ha sido sobrepasado en un porcentaje 10% del tiempo o período de medición T. . Es un indicador de los valores más altos de la señal.
- `LA50`: Indican que el nivel de presión sonora LA ha sido sobrepasado en un porcentaje 50% del tiempo o período de medición T, pudiendo utilizarse en ocasiones como un valor medio del nivel de presión sonora medido.
- `LA90`: Indican que el nivel de presión sonora LA ha sido sobrepasado en un porcentaje 90% del tiempo o período de medición T. Se utiliza como indicador del nivel del ruido de fondo.
- `LA99`: Indican que el nivel de presión sonora LA ha sido sobrepasado en un porcentaje 99% del tiempo o período de medición T.
- `LAMax`: El valor máximo de nivel de presión sonora alcanzado durante todo el intervalo de estudio.
- `LAMin`: El valor mínimo de nivel de presión sonora alcanzado durante todo el intervalo de estudio.
- `TimeInstant` : La fecha y la hora del envío del dato del agente IoT.


##### Atributos para realizar cálculos / lógica de negocio

- `eveningLAEqLimit`: dB en el tramo de la tarde a partir de los cuales se marca en rojo un sensor de ruido
- `eveningTimeLimit`: Define la hora fin del tramo de la tarde
- `morningLAEqLimit`: dB en el tramo de la mañana a partir de los cuales se marca en rojo un sensor de ruido
- `morningTimeLimit`: Define la hora fin del tramo de la mañana
- `nightLAEqLimit`: dB en el tramo de la noche a partir de los cuales se marca en rojo un sensor de ruido
- `nightTimeLimit`: Define la hora fin del tramo de la noche
- `calculatedTimestamp`: Define el momento en el que la entidad ha sido actualizada con todos los datos calculados

##### Atributos para el GIS / Representar gráficamente la entidad

- `noiseLevelLAEq`: Para el GIS. Posibles valores: verde, rojo, gris. Verde implica estar por debajo del límite para el tramo horario. Rojo implica estar por encima del límite en el tramo horario. Gris significa sin datos recientes.

##### Atributos para la monitorización

- `operationalStatus`: Posibles valores: ok, noData
- `maintenanceOwner`: Responsable técnico de esta entidad
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

##### Atributos innecesarios en futuras entidades

- `dateModified`: Timestamp de actualización del dato (ver documentación diseño C10 para entenderlo mejor)

## Lista de entidades que implementan este modelo (23 entidades)

| Subservicio | ID Entidad                   |
| ----------- | ---------------------------- |
| /sonometros | NoiseLevelObserved-HOPVLCi1  |
| /sonometros | ...                          |
| /sonometros | NoiseLevelObserved-HOPVLCi22 |
| /sonometros | NoiseLevelObserved-HOPVLCi30 |



##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/NoiseLevelObserved-HOPVLCi1' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /sonometros' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "3387",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/sonometros",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "24ae3d44-38d4-11ed-9aad-0a580a810238; cbnotif=2; node=2I_aNjd2H1; perseocep=2071940; cbnotif=3",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"NoiseLevelObserved-HOPVLCi19\",\"type\":\"NoiseLevelObserved\",\"LA1\":{\"type\":\"Number\",\"value\":80.868331909,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LA10\":{\"type\":\"Number\",\"value\":71,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LA50\":{\"type\":\"Number\",\"value\":62.607841492,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LA90\":{\"type\":\"Number\",\"value\":61.414894104,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LA99\":{\"type\":\"Number\",\"value\":60.956249237,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LAMax\":{\"type\":\"Number\",\"value\":86.569999695,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LAMin\":{\"type\":\"Number\",\"value\":50.200000763,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"LAeq\":{\"type\":\"Number\",\"value\":61.931770325,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"Carrer Polo y Peyrolón, 53\",\"metadata\":{}},\"calculatedTimestamp\":{\"type\":\"Timestamp\",\"value\":\"2022-09-20T11:04:48Z\",\"metadata\":{}},\"dateModified\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:04:48.000Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"dateObservedFrom\":{\"type\":\"Text\",\"value\":\"2022-09-20T11:03:49Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"dateObservedTo\":{\"type\":\"Text\",\"value\":\"2022-09-20T11:04:48Z\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-20T11:05:31.798Z\"}}},\"eveningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"eveningTimeLimit\":{\"type\":\"Number\",\"value\":\"23\",\"metadata\":{}},\"frequencies\":{\"type\":\"StructuredValue\",\"value\":{\"100\":40,\"125\":40,\"160\":40,\"200\":40,\"250\":40,\"315\":40,\"400\":40,\"500\":40,\"630\":40,\"800\":40,\"1000\":40,\"1250\":40,\"1600\":40,\"2000\":40,\"2500\":40,\"3150\":40,\"4000\":40,\"8000\":40,\"10000\":40},\"metadata\":{}},\"location\":{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-0.3543668,39.474628]},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"measurand\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"morningLAEqLimit\":{\"type\":\"Number\",\"value\":65,\"metadata\":{}},\"morningTimeLimit\":{\"type\":\"Number\",\"value\":\"21\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"Sensor 19 - Carrer Polo y Peyrolon 53\",\"metadata\":{}},\"nightLAEqLimit\":{\"type\":\"Number\",\"value\":55,\"metadata\":{}},\"nightTimeLimit\":{\"type\":\"Number\",\"value\":\"7\",\"metadata\":{}},\"noiseLevelLAEq\":{\"type\":\"Text\",\"value\":\"verde\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"Ayto. Valencia\",\"metadata\":{}},\"project\":{\"type\":\"Text\",\"value\":\"impulsoVLCi-red.es\",\"metadata\":{}},\"refDevice\":{\"type\":\"Text\",\"value\":\"Device-HOPVLCi19\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"sonometerClass\":{\"type\":\"Integer\",\"value\":2,\"metadata\":{}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "NoiseLevelObserved-HOPVLCi19",
        "type": "NoiseLevelObserved",
        "LA1": {
          "type": "Number",
          "value": 80.868331909,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LA10": {
          "type": "Number",
          "value": 71,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LA50": {
          "type": "Number",
          "value": 62.607841492,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LA90": {
          "type": "Number",
          "value": 61.414894104,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LA99": {
          "type": "Number",
          "value": 60.956249237,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LAMax": {
          "type": "Number",
          "value": 86.569999695,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LAMin": {
          "type": "Number",
          "value": 50.200000763,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "LAeq": {
          "type": "Number",
          "value": 61.931770325,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-20T11:05:31.798Z",
          "metadata": {}
        },
        "address": {
          "type": "Text",
          "value": "Carrer Polo y Peyrolón, 53",
          "metadata": {}
        },
        "calculatedTimestamp": {
          "type": "Timestamp",
          "value": "2022-09-20T11:04:48Z",
          "metadata": {}
        },
        "dateModified": {
          "type": "DateTime",
          "value": "2022-09-20T11:04:48.000Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "dateObservedFrom": {
          "type": "Text",
          "value": "2022-09-20T11:03:49Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "dateObservedTo": {
          "type": "Text",
          "value": "2022-09-20T11:04:48Z",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-20T11:05:31.798Z"
            }
          }
        },
        "eveningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "eveningTimeLimit": {
          "type": "Number",
          "value": "23",
          "metadata": {}
        },
        "frequencies": {
          "type": "StructuredValue",
          "value": {
            "100": 40,
            "125": 40,
            "160": 40,
            "200": 40,
            "250": 40,
            "315": 40,
            "400": 40,
            "500": 40,
            "630": 40,
            "800": 40,
            "1000": 40,
            "1250": 40,
            "1600": 40,
            "2000": 40,
            "2500": 40,
            "3150": 40,
            "4000": 40,
            "8000": 40,
            "10000": 40
          },
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [-0.3543668, 39.474628]
          },
          "metadata": {}
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "measurand": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "morningLAEqLimit": {
          "type": "Number",
          "value": 65,
          "metadata": {}
        },
        "morningTimeLimit": {
          "type": "Number",
          "value": "21",
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "Sensor 19 - Carrer Polo y Peyrolon 53",
          "metadata": {}
        },
        "nightLAEqLimit": {
          "type": "Number",
          "value": 55,
          "metadata": {}
        },
        "nightTimeLimit": {
          "type": "Number",
          "value": "7",
          "metadata": {}
        },
        "noiseLevelLAEq": {
          "type": "Text",
          "value": "verde",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "Ayto. Valencia",
          "metadata": {}
        },
        "project": {
          "type": "Text",
          "value": "impulsoVLCi-red.es",
          "metadata": {}
        },
        "refDevice": {
          "type": "Text",
          "value": "Device-HOPVLCi19",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "sonometerClass": {
          "type": "Integer",
          "value": 2,
          "metadata": {}
        }
      }
    ]
  }
}
</details>

# Atributos que van al GIS para su representación en una capa

Al GIS sólo van las entidades que cumplen el siguiente patrón en su ID:
- Hopu: ^NoiseLevelObserved-HOPVLCi[0-9]+$
- Ruzafa: ^T[0-9]{6}$
- BSG: ^[0-9]{4}$


##### Atributos de la entidad context broker

- id
- type

##### Atributos descriptivos de la entidad

- name
- location
- address
- project

##### Atributos de la medición

- dateObservedFrom
- dateObservedTo
- LAeq
- LA99
- LA90
- LAMax
- LAMin
- LA1
- LA50
- LA10

##### Atributos para el GIS / Representar gráficamente la entidad

- noiseLevelLAEq

##### Atributos para la monitorización

- operationalStatus
- maintenanceOwner
- maintenanceOwnerEmail
- serviceOwner
- serviceOwnerEmail

##### Atributos innecesarios en futuras entidades

- Ninguno
