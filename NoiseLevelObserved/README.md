Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/spec.md) que se tiene. Contiene las entidades del proyecto Impulso, componente C10

Otras especificaciones históricas
===
Entidades de BSG de Tossal y Serranos de periodicidad minutal [Especificación BSG](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/archive/spec-bsg-tossal-serrano.md)

Entidades de Elecnor para el barrio de ruzafa de periodicidad minutal [Especificación Ruzafa](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/archive/spec-elecnor-ruzafa.md)

Entidades de Elecnor para el barrio de ruzafa de periodicidad diaria [Especificación Ruzafa](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/archive/spec-elecnor-ruzafa-daily.md)

Entidades para sonómetros de las zonas ZAS de periodicidad minutal [Especificación ZAS](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/archive/spec-zas.md)

Entidades para sensores de ruido de la Diputación de Valencia de periodicidad quince minutal [Especificación DipuVal](https://gitlab.com/vlci-public/models-dades/environment/-/blob/main/NoiseLevelObserved/archive/spec-dipuval.md)